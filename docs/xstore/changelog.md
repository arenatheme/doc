---
metaTitle: XStore - 1.1 Update Change Log - ArenaCommerce Help Center
sidebarDepth: 2
---
# XStore Update 1.1 Change Log  

## NEW Exciting Themes!  
- Home Decor  
- Niche Market  
- Agricultural  

## NEW FEATURE Custom Coming Soon Templates!
After installing XStore, choose one of the following Coming Soon template and contact our team. 
We will put it into your theme for FREE!
- X Coming Soon  
- Coming Soon Flat  
- Coming Soon Red  
- Coming Soon White  
- Coming Soon Black  
- We are Coming Soon  
- Things are Coming  
- Space Coming  
- ModernX Coming  
- Coming Soon XStore  

## New options  
- Elegant fade-in mobile menu.  
![](/MobileMenu.gif)
- Sidebar cart pop-up.  
![](/Sidebar.gif)  

## Bug fixes.  

## Level up your Marketing game with GROWAVE!
- **30-day extended free trial** for all packages when install with our promo [link](https://growave.io?ref=arenacommerce).
::: warning Themes Supported:
- Electro 6.0+
- Xstore 1.0+
- Handy 4
- Zeexo 1+
:::
:::: tabs
::: tab "Installing Growave" id="growave-app-settings"
1. From your myShopify admin homepage, click on `App`.
2. Click on our affiliated [link](https://growave.io?ref=arenacommerce) to enable your discount!
![get-discount-growave](/get-discount-growave.png)
3. Choose the package you want to purchase.
![growave-trial](/growave-trial.png)
4. Type in your store handle.
![growave-storehandle](/growave-storehandle.png)  
5. Log into your Shopify store.
6. Click `Install App`.
![growave-install](/growave-install.png)
7. Click `Start free trial` on the package you want to try.
![growave-free-trial](/growave-free-trial.png)
8. Approve Growave charge by clicking `Start free trial`.
![growave-approval](/growave-approval.png)
9. Growave will then approve your purchase. Enjoy your 30-day-trial!
![growave-confirm](/growave-confirm.png)
10. You can now start using Growave by clicking `Go to the Dashboard`!
![start-using](/start-using.png)
:::
::: tab "Theme Settings configuration" id="growave-theme-setting"
1. In the Theme Customization Homepage > click `Theme settings` > `App integration` > `Product review`.  
2. In `Product review options`, choose as follow:
- [ ] None: Disable / Hide product review.
- [ ] Product Review: Use Shopify Product Reviews app.
- [ ] Loox - Photo Reviews: Use Loox app.  
- [x] Growave: Use Growave app.
3. Click `Save`.
:::
::::  