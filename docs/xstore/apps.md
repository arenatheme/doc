---
metaTitle: XStore - Integrating Shopify App into XStore Theme - ArenaCommerce Help Center
sidebarDepth: 3
---
# Integrating Shopify App
If you're familiar with our catalogue of Shopify themes, then you probably are familiar with the integrable Shopify Apps that we support depends on the theme you purchased. Here are some apps and the list of their compatible themes, along with integration details to help boost your customer experience and conversion.

## Wishlist & Compare <Badge text="2.0"/>
- Price: Free.  
::: warning Themes Supported:
- Handy 4.2+
- Zeexo 1.1+
- Electro 6.0+
- Xstore 1.0+
:::

:::: tabs
::: tab "Installing Wishlist & Compare App" id="wishlist-compare-install"
1. Go to [Wishlist & Compare](https://apps.shopify.com/wishlist-compare).
2. Click `Add App`.
3. Sign into your store and accept permission to Install the app.
:::
::: tab "Theme Settings configuration" id="wishlist-&-compare-theme-settings"  
1. In the Theme Hompage > click `Theme settings` > `App integration` > `Wishlist & Compare`.  
2. Choose as follows: `Product review options` choose `Wishlist Compare app`.  
3. [x] Enable Compare.  
4. Click `Save`.
:::
::::  

:::tip In case you prefer a third-party app
- It is advisable to delete these codes after uninstalling Arena Wishlist & Compare app to help the other app integrate better with the theme. These codes are:  
```html
{% include 'arn_wl_cp_settings' %}  
{% include 'arn_wl_cp_styles' %}  
{% include 'arn_icons_define' %}
```  
- They can be found in the `Actions` or `Theme actions` button's drop down list when clicked.  
  - `myShopify Admin Homepage` > `Online Store` > `Themes` > `Current theme` > `Actions` > `Edit code`.
![edit-code](edit-code.png)  
  - Theme customization page > `General settings` > `Theme actions` > `Edit code`.
![edit-code2](edit-code2.png)  
- Once in the `Edit code` window, choose `{\}theme.liquid` and paste any line of the codes above into the search box.  
- These codes always go together and you just need to delete them from `{\}theme.liquid` the way you delete text in any document processor. 
- When done, click `Save`.
:::  

## Product Review
::: warning Themes Supported:
- All of our Shopify themes.
:::

### 1. Product Reviews by Shopify <Badge text="Free"/>
- Price: Free.
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: true }"
::: tab "Installing Shopify Review App" id="shopify-product-reviews-app"
1. From your myShopify Admin Homepage, click on `App`.
2. Click on the `Visit the Shopify App Store` > search for [Product Reviews by Shopify](https://apps.shopify.com/product-reviews).
3. Click `Add app`.
4. Log into Shopify App Store.
5. Confirm app installation.
:::
::: tab "Theme Settings configuration" id="shopify-product-reviews-theme-settings"
1. In the Theme Customization Homepage > click `Theme settings` > `App integration` > `Product review`.  
2. In `Product review options`, choose as follow:
- [ ] None: Disable / Hide product review.
- [x] Product Review: Use Shopify Product Reviews app.
3. Click `Save`.
:::
::::


### 2. Loox - Photo Reviews <Badge text="20% off"/>
- Price: $9.99/month - 20% off when install with our promo [link](https://loox.io/app/Arena).
::: warning Themes Supported:
- Zeexo 1.0+
- Electro 6.0+
- Xstore 1.0+
:::
:::: tabs
::: tab "Installing Loox" id="loox-app-settings"
1. From your myShopify admin homepage, click on `App`.
2. Click on the `Visit the Shopify App Store` > search for [Loox - Photo Reviews](https://loox.io/app/Arena).
3. Click `Add app`.
4. Log into Shopify App Store.
5. Confirm app installation.
:::
::: tab "Theme Settings configuration" id="loox-theme-setting"
1. In the Theme Customization Homepage > click `Theme settings` > `App integration` > `Product review`.  
2. In `Product review options`, choose as follow:
- [ ] None: Disable / Hide product review.
- [ ] Product Review: Use Shopify Product Reviews app.
- [x] Loox - Photo Reviews: Use Loox app.
3. Click `Save`.
:::
::::  

### 3. Growave <Badge text="30 days free trial extended!"/>
- Price: Free to $299.99/month with **30-day free trial** for all packages when install with our promo [link](https://growave.io?ref=arenacommerce).
::: warning Themes Supported:
- Electro 6.0+
- Xstore 1.0+
:::
:::: tabs
::: tab "Installing Growave" id="growave-app-settings"
1. From your myShopify admin homepage, click on `App`.
2. Click on our affiliated [link](https://growave.io?ref=arenacommerce) to enable your discount!
![get-discount-growave](/get-discount-growave.png)
3. Choose the package you want to purchase.
![growave-trial](/growave-trial.png)
4. Type in your store handle.
![growave-storehandle](/growave-storehandle.png)  
5. Log into your Shopify store.
6. Click `Install App`.
![growave-install](/growave-install.png)
7. Click `Start free trial` on the package you want to try.
![growave-free-trial](/growave-free-trial.png)
8. Approve Growave charge by clicking `Start free trial`.
![growave-approval](/growave-approval.png)
9. Growave will then approve your purchase. Enjoy your 30-day-trial!
![growave-confirm](/growave-confirm.png)
10. You can now start using Growave by clicking `Go to the Dashboard`!
![start-using](/start-using.png)
:::
::: tab "Theme Settings configuration" id="growave-theme-setting"
1. In the Theme Customization Homepage > click `Theme settings` > `App integration` > `Product review`.  
2. In `Product review options`, choose as follow:
- [ ] None: Disable / Hide product review.
- [ ] Product Review: Use Shopify Product Reviews app.
- [ ] Loox - Photo Reviews: Use Loox app.  
- [x] Growave: Use Growave app.
3. Click `Save`.
:::
::::  


## Search Bar
### Smart Search & Instant Search App <Badge text="30% off"/>
 - Price:  
   * Free for stores with less than 25 products.  
   * For stores with 25 products or more, price starting from $6.30/month, with 30% off when install with our promo link.
::: warning Themes Supported:
- All of our Shopify themes.
:::
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: true }"
::: tab "Installing Smart Search & Instant Search" id="smart-setting"
1. Install app via our promo [link](http://www.searchanise.com/get/arenacommerce) (30% off).  
2. Click `Add app`.  
3. Log into Shopify App Store.  
4. Confirm app installation.
:::
::: tab "Theme Settings configuration" id="theme-settings"
1. Go to Smart Search & Instant Search app's dashboard > click `Instant search widget` on the second row below 'Dashboard'.
2. Select `Enable instant search widget` &  `Optimize for phones and tablets` > click `Apply changes`.
![](/search_instant.png).
3. In the Theme Hompage > click `Theme settings` > `App integration` > `Search box`.
- [x] Show Search Box: Show search box in the header.
- - Auto Suggestion Search by
    - [ ] None (This option disables auto suggestion in search box).
    - [ ] Default (This option uses auto suggestion by code build-in theme).
    - [x] Smart Search app (This option uses Smart Search & Instant Search by Searchise).
4. Click `Save`.
:::
::::

## Webpage Translation Service
### Weglot  
Weglot is a tool to help your store goes multilingual. It is an all-in-one platform that offers translation of **100+ languages** worldwide and allows both automatic and human translation, along with access to professional translators (so you never have to muscle the job alone!) and team collaboration directly inside Weglot.  
For more details, visit them [here](https://weglot.com/features/)
::: warning Themes Supported:
- All of our Shopify themes.
:::
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: true }"
::: tab "How to get Weglot API" id="theme-settings"

@[youtube](IXRlYNOeLDA)

***Please contact support@arenathemes.com to get coupon discount of 20% when you upgrade to Paid plan***

:::
::: tab "Weglot Register & Settings" id="weglot-setting"
After signing up, you have a 10-day free trial period to test Weglot. Once the 10-day free trial ends, you have 2 options:
- If your website has less than 2000 words and you want to translate it only into one language, you can use the Free plan. Free plan is automatically chosen at the end of the trial period, so you don't need to choose it manually.
- If your website has more than 2000 words, you will need to upgrade your plan to continue using Weglot.
You can find your number of translated words in your account, on your dashboard.
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/5d0a37de2c7d3a1cad5b8f13/file-VIzht1Oy3D.png)
Detailed pricing:
![](/xstore/weglot-price.png)
:::
::::

You can also find all links to our affiliate discount on the `Member Perk` tab in our Arena Dashboard Shopify App locate in the `App` section of your myShopify Admin Homepage.
![Discount links](/xstore/apps-promo-links.png)


## End of Support App

### 1. Judge.me <Badge text="End of Support (January 2020)" type="error"/>
::: warning Themes Supported:
- Electro 5.x
- Handy 4.x
- Zexxo 1.x
- Chromium 2.x
:::
:::: tabs
::: tab "Judge.me" id="judgme-app-installation"
:::
::: tab "Theme Settings" id="judgme-theme-setting"
1. In the Theme Customization Homepage > click `Theme settings` > `App integration` > `Product review`.  
2. In `Product review options`, choose as follow:
- [ ] None: Disable / Hide product review.
- [ ] Product Review: Use Shopify Product Reviews app.
- [x] Judge.me: Use "Judge.me" app.
- [ ] Loox: Use Loox app.
:::
::::

### 2. Wishlist & Compare 1.0 <Badge text="End of Support (January 2020)" type="error"/>
<span style="color:red">*The new Wishlist & Compare 2.0 is now available. Please refer to its details in the beginning above.*</span>
::: warning Themes Support:
- From 04/2020, we had stopped supporting this app. Don't install it.
- Price: Free.
:::
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Installing Arena Wishlist & Compare App" id="wishlist-compare-settings"
It's our free Shopify app to support Customers can create a Wishlist by logging into their store account and simply start saving their favorites to it for later viewing.
Install URL: [Arena Wishlist Compare](https://apps.arenatheme.com/install)
Wishlist & Compare is not offered by Shopify as a function so we have built a Free Shopify Application to assist customers.
- **Wishlist** is build by using Shopify App Proxy, it's not required to setup template page on your Shopify store. Wishlist function will launch at https://your-shopify-url/apps/wishlist.
- **Compare** required to create and assign **a separate page** for the Compare page display
1. Go to [https://apps.arenatheme.com/install](https://apps.arenatheme.com/install).
2. Input your Shopify Store url to the form and click `Install`.
![](/wishlist_install.png).
4. Sign in your store and accept permission to Install app.
5. From your Shopify admin, go to `Apps` > `Arena Wishlist Compare`. Please ensure that you Wishlist & Compare app setting at `Enable` status.
![](/status_wc.png)
:::
::: tab "Arena Wishlist Compare Settings" id="wishlist-setting"
Wishlist & Compare is not offered by Shopify as a function so we have built a Free Shopify Application to assist customers.
- **Wishlist** is build by using Shopify App Proxy, it's not required to setup template page on your Shopify store. Wishlist function will launch at: https://your-shopify-url/apps/wishlist.
- Compare required create and assign a separate page for the Compare page display.
:::
::: tab "Theme Settings" id="wishlist-theme-settings"
- [x] Enable Wishlist.
- [x] Enable Compare.
:::
::::