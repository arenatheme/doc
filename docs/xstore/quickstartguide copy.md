---
metaTitle: XStore - Quick Start Guide - ArenaCommerce Help Center
sidebarDepth: 2
---
# Quick Start Guide

Starting an online store on one's own can be the experience of a lifetime. We understand that it can be quite a handful familiarizing yourself with Shopify & Xstore as there are a lot to learn, especially if you has no background in information technology.  

In light of this, we have prepared a starter pack full of useful information and guidelines including help pages, easy-to-access online documentation, pdf guide for offline access and video tutorials. You will also see the links to articles containing frequently encountered problems along with some tips to help make the learning process easier and more enjoyable for you.  

Please never hestitate to use the live search function at the center top of the page, as most of the time the answers to your questions had been answered in one of our posts or videos. If you are unable to find an answer to your questions, please check our [**Before You Submit A Ticket**]() page first before submitting a support ticket [here](https://arenathemes.freshdesk.com/support/tickets/new) or clicking `Support` button in Arena Dashboard App you installed in your myShopify. After your submission, one of our customer support agent will personally assist you within 1 to 3 business days.


## General And Important Information  
Before diving deep into the nuts and bolts of starting an exciting online business, please take a look at the outline below to assist with your navigation and learning.  

---
- [XStore Information Portal](/xstore/quickstartguide.md)  
---
- [XStore Video Tutorials Portal](https://www.youtube.com/playlist?list=PLsZwfcHiP6N_-a6zBgCezaCF2zVCq4T4E)  
--- 
- [XStore Theme Showcase](https://landing.arenacommerce.com/xstore/)  
---
- [Frequently Asked Questions and How Tos](/faq/README.md)  
---
- [Support Guidelines & Policies](/quickstart/support.md#_3-support-policy)
---  


## Beginner Essentials
In this section, we will list the most essentials a new Shopify store owner needs to study to bring their store public smoothly. While you can always hire professional helps from us and the Shopify team, it is best to take matters to your own hands or at least know what should be done to ensure productivity and time-saving.  
Some of the posts we included are warmly provided by Shopify and you may have come across them. Some are the results from thousands of feedbacks from our treasured customers that we think will be of great value to you.  
Enjoy!  

---
- [Beginner's Overview of the myShopify Admin Homepage](https://help.shopify.com/en/manual/intro-to-shopify/shopify-admin/shopify-admin-overview)  
---
- [Essential Setups For A New Store](https://help.shopify.com/en/manual/intro-to-shopify/initial-setup)  
---
- [Installation Requirements](/xstore/themeinstall.md#step-1-installing-required-app-and-extension)  
---
- [Installing XStore Theme](/xstore/themeinstall.md)  
---
- [Begin Customizing Your Theme](/xstore/commonterms.md)   
---
- [Get The Best Out Of XStore Customization To Boost Your Business]()  
---
- [How XStore Can Help Develop Your Business In The Long Run]()  
---
- [Store Aesthetic - Design Tricks From Top Brands]()  
---
- [Saving The Most For Your Business With Our Partners]()  
---
- [Go Multilingual With Weglot](/xstore/apps.html#weglot)
---  



## Installing XStore Theme  
---
- [Locating Your Theme Purchase Code](/xstore/purchase-code)  
---
- [Installing Required App and Extension](/xstore/themeinstall.md#step-1-installing-required-app-and-extension)  
---
- [Adding Your License Code to Activate The Theme Installation Process](/xstore/themeinstall.md#step-2-adding-your-license-code)  
---
- [Install the theme](/xstore/themeinstall.md#step-4-install-the-theme)  
---
- [How To Upload XStore Demo Shop To Your Shopify Account](/xstore/README.md)  
---



## Update & Maintenance
The update process will be similar to other themes updates, which you can take a look [here](/electro5/update.md#electro-5-update-to-electro-6).  
We will include update instructions for further versions of XStore upon release.  
For further information, please go [here](/xstore/update.md).

## Backup Your Store
You may think that Shopify backs up your store, but in reality, **Shopify only backs up its platform**. You are only one mistake, bug, or worst - attack away from unwanted downtime, lost revenues, customers outrage and an expensive rebuild. Our friends at [Rewind](https://rewind.io/backup/shopify/) offer ingenious solutions to this problem with their brilliant backup plan.
From Basic plan of only $3/month to Enteprise plan for your million-dollar business, they have got you covered!  

You may also want to **duplicate your theme first** before customizing from comparison purposes to data preservation. For that, Shopify had already has an intuitive, easy-to-do article [here](https://help.shopify.com/en/manual/using-themes/managing-themes/duplicating-themes).  