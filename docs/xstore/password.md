---
metaTitle: XStore - Password Page - 'Coming Soon' Page Configuration - ArenaCommerce Help Center
sidebarDepth: 2
---  
# :sparkles: NEW Password - 'Coming Soon' Page Configuration  

For XStore update 1.1 and after, the Password - or 'Coming Soon' Page as we would like to call them, are now **FREE** for your purchased XStore theme. Although we intended to introduce this page for new businesses which will have their first launch online, you can also request us to add this for you when you upgrade your theme. Just choose one of the following 'Coming Soon' Page templates and contact us, we will add the page to your store free of charge.  

- X Coming Soon  
- Coming Soon Flat  
- Coming Soon Red  
- Coming Soon White  
- Coming Soon Black  
- We are Coming Soon  
- Things are Coming Soon  
- Space Coming  
- ModernX Coming  
- Coming Soon XStore  

Demo for these pages are available in our live demonstration here: ()  

To enable this page, from your **myShopify Admin Home Page**, simply go to **`Online Store`** > **`Preferences`**.  
Scroll down to **Password protection** and tick **:white_check_mark: Enable password**.  

After this step, please choose your ideal 'Coming Soon' Page through our demo contact us, we will add the page to your store free of charge.  

Once the page has been added to your store, you can start configure the page by choosing **Password page** from the top drop down menu in the **Theme Editor**.  
Then click **Password Content** in the **Sections** tab to open the customizing menu.  
![](/Password1.png)  

The first **`LAYOUT`** option is used to configure the layout for the whole page. As with normail page, you can either choose a **:white_check_mark: Boxed layout**, or untick for a full screen experience.  
![](/Password2.png)  

At the end of the **`LAYOUT`** drop down menu is the **Header text color** configuration. 
Changing this color will change the **Enter using password** text's color.  
![](/Password3.png)  
![](/Password4.png)  

Scrolling down to the **`CONTENT`** section, you can choose to modify and customize **Banner** - the section displaying the galaxy image you see in the first image.  
![](/Password5.png)  
![](/Password6.png)  

**Countdown date** - this one allows you to set up the countdown until your store's official online launch.  
![](/Password7.png)  
![](/Password8.png)  
|     |  Countdown style guide  |     |
| :-: | :-: | :-: |
| Style 1 | Style 2 | Style 3 |  
| ![](/CCountdown1.png) | ![](/CCountdown2.png) | ![](/CCountdown3.png) |  
You can configure the countdown's style in the slider bar at the beginning of the **COUNTDOWN STYLE** drop down menu.  

**Logo** - this one allows you to import and configure the logo's display on this page.  
![](/Password16.png)  

**Text** - this one allows you to add and configure text to convey your message.  
For this demo, we have added 2 **Text** sections and marked them incorresponding colors so you can see the message each of them bears.   
|     |     |
| :-: | :-: |
| ![](/Password9.png) | ![](/Password10.png) |  
:::warning Since these sections work like textboxes 
Each section configuration only allows one kind of font and size, you can add more of these sections by clicking **Add content** at the bottom of the **Password Content** mega tab and choose **Text**. A new similar section will appear for you to add new text.  
![](/Add-cont.png)  
:::  

### Adding Social Media Icons  
To increase anticipation and engagement, we also provide you an option to put social media link to the 'Coming Soon' Page.  
In the **Add content** drop down menu, simply choose **Group Social icon**.  
The sample icons will then appear under the **Banner** section. You can nest them up as seen in the image below by choosing **:white_check_mark: Block nested** in the beginning of the **BLOCK SETTINGS** drop down tab.  
![](/Password15.png)  
![](/Password13.png)  
To link these icons, simply put in the corresponding social media link under each icon's configuration tab.  
![](/Password14.png)  

:tada: And that is all for this tutorial :tada:  
We wish you a successful launch to begin your online business adventure!  
