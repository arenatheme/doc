---
metaTitle: XStore - Handling Duplicated Data - ArenaCommerce Help Center
sidebarDepth: 2
---
# How to Delete Duplicated Data
While installing the theme, you may come across a duplicated materials error.  

This error happens when there was a previous unsuccessful theme install attempt or there simply are duplicated assets between installations. When you install a new theme, even if you install the same as the old one, Shopify will automatically recognize the new theme as a different entity than the old theme. Trouble is, Shopify does not recognize the new assets(materials that dictate collections, articles and page) that have the same name as the old one - as different entities. This enables the error since it cannot overwrite the old files with the same name with the new files being installed.    

In case you have installed a theme with similar assets, [making a backup version of the current one](https://help.arenacommerce.com/xstore/quickstartguide.html#backup-your-store) then wipe the materials from your myShopify before would be a great idea to start thing anew.  

To begin the process, please follow the steps below:

## Step 1: Remove The Theme  
You can only remove the theme which is not published. To remove the theme:  
- Go to the `Arena Dashboard` app in the `App` section of your myShopify Admin Homepage > choose the `Theme Installation` tab > scroll down to the list of `Installed Themes` > Click `More actions...` button on the theme you want to remove > Click `Remove`.  
![remove-theme](/remove-theme.png)  

## Step 2: Delete Blog Posts  
To delete blog posts
- Go to the `Online Store` section of your myShopify Admin Homepage > choose the `Blog posts` tab > choose the ones you want to remove > Click `More actions...` > Click `Delete blog posts`.  
You can also choose to add or remove tags here.
![delete-blog-posts](/delete-blog.png)  

## Step 3: Delete Pages  
To delete pages
- Go to the `Online Store` section of your myShopify Admin Homepage > choose the `Pages` tab > choose the ones you want to remove > Click `More actions...` > Click `Delete pages`.
![delete-page](/delete-page.png)  

## Step 4: Remove Navigations  
- Removing navigation is a little trickier than deleting blog posts and page. To start, first you will need to access Navigation through the `Navigation` tab in `Online Store` section of your myShopify Admin Homepage.  
- Upon clicking `Navigation`, the `Navigation` window will appear in a new tab. This is also the same window you need to click `Arena Menu Import` when installing the theme.  
There is no Delete button next to each Menu, so you will have to click on each one to get to their own page. For this tutorial, I will delete the 'Corporate Main Menu' as seen in the picture below.  
![delete-menu](/delete-menu1.png)  
- When the menu page loaded, click `Delete menu` at the end.  
![delete-menu2](/delete-menu2.png)
- When the dialogue box appear, click `Delete menu` again.
![delete-menu3](/delete-menu3.png)  
- Once the menu was deleted, the site will take you back to the Navigation page. Continue deleting other Menu the same way until you have deleted all the menu belongs to the theme you want to remove. A successful notification will also be displayed after each deletion.  
![delete-menu4](/delete-menu4.png)
For the original Demo Shops, the menu name hints which theme it belongs to. Once you have customized the menu it would be much more easier to identify them.  

## Step 5: Delete duplicated files  
To do this, we will need to go to a different section of your myShopify Admin Homepage.  
- Go to `Settings` at the lower left corner of the Homepage > choose `Files`.
![setting-file](/setting-file.png)  
- Choose the ones you want to delete > Click `Actions` > Click `Delete selected files`.
![delete-file](/delete-file.png)
A successful notification will also be displayed after each deletion.  