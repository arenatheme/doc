---
metaTitle: XStore - Locating Your Theme Purchase Code - ArenaCommerce Help Center
sidebarDepth: 2
---

# Locating Your Theme Purchase Code  
After purchasing our theme, you will be given the purchase code to install the theme into your Shopify. Never worry, we will get to the installation process in the next article!  

**Step 1**
- Log into your ThemeForest (Envato) account and hover above your username in the top right corner to access the dropdown list. Select `Downloads`.  

**Step 2** 
- Find the XStore theme in the list of items you have bought.  
You can also find the link to the XStore theme [here](https://themeforest.net/item/xstore-premium-multipurpose-shopify-theme/26492260)  

**Step 3**
- Click the `Download` button next to the theme.  
From the drop down menu, choose `License certificate & purchase code` (available as PDF or text file). It is better to download the text file so you can copy and paste the purchase code when adding the license key to Shopify.  
Open the file and you will find the purchase code.  
<div class="video-wrapper">
<iframe class="wistia_embed" src="//fast.wistia.net/embed/iframe/gqsrs2assi" name="wistia_embed" width="640" height="455" frameborder="0" allowfullscreen="allowfullscreen" sandbox="allow-scripts allow-forms allow-same-origin allow-presentation"></iframe>
</div>  
- Here is an example of a PDF License certificate and purchase code:
![](https://help.market.envato.com/hc/en-us/article_attachments/201801083/License_Purchase_Code_Example.png)
[Source](https://arenathemes.freshdesk.com/a/solutions/articles/6000116407-how-to-find-your-themeforest-item-purchase-code)  

Now it's time to prepare your Shopify store for theme installation. Please click [Preparing your Shopify Store](/xstore/themeinstallprep) for the next step.  