---
metaTitle: Xstore Demo Shop Styles - ArenaCommerce Help Center
sidebarDepth: 2
---
# Get Started Quickly with Xstore Demo Shop Styles!
Upon finish installing the theme and required apps, time to start building your dream store!  
The first step we need to do is uploading theme package. While you can do that manually, we recommend you start from one of the Demo Shop Styles that we have included in your purchase.

## Using Xstore Demo Shop as a starting point will help you
- Familiarize yourself with XStore.
- Copy a website you have in mind to ease out the learning process.
- Get your shop live ASAP!  

**The main factors that you should consider when choosing a Demo Shop**
- Choose the Demo Shop that looks closest to the layout you'd like your shop to have.
- Remember that you can **mix & match everything you see in the Demo**, e.g. it is easy to keep a layout like Home 1 & Home 2 but give it a different color palette etc...  

**One main factor you should consider when choosing a Demo Shop**  
- If your industry has already been represented by one of the Demo Shop, choose that one so you will be able to quickly populate your own shop with relevant images and text. You can always change the colors and layout but you will find it most helpful to already have a shop-appropriate content built right in from the beginning!
- The Xstore theme takes 'theme styles' a step further by allowing you to **import the styles — in full!** when you download the theme. This means you can import all of the settings, contents, sections, products, collections, placeholder images and even navigation (including mega navigation) as seen in the XStore Demo Shops into your own shop.  
***<span style="color:indigo">So, picking one that looks closest to the layout you'd like your shop to have is typically the best route to go!</span>***  
- Although it is an one-theme package, we can change theme settings to modify the basic one into 68+ Demo Shops! We also made one theme into some several versions of itself in order to fit sample demonstration contents to fit the following layouts:  
        - FASHION  
        - LINGERIE  
        - BABY  
        - SHOES  
        - GROCERY  
        - ONE PAGE  
        - ELECTRONICS  
        - FURNITURE  
        - HANDMADE  
        - FLOWERS  
        - WEDDING  
        - EVENT  
        - STUDIO  
        - GAME  
        - BOOK  
        - PETS  
        - ENGINEER  
        - SPORT  
        - PLUMBING  
You can take a look at each preset showcase in the Xstore Landing Page [here](https://landing.arenacommerce.com/xstore/).

## Before customizing your theme
You need to install it first! If you have not read the previous articles, please follow the step below to prepare your shop for customization.  
**Step 1:** 
[<button class="btn"><i class="far fa-arrow-alt-circle-right"></i>Preparing Your Shopify Store</button>](/xstore/themeinstallprep)  
**Step 2:** 
[<button class="btn"><i class="far fa-arrow-alt-circle-right"></i>Install the XStore Theme</button>](/xstore/themeinstall)  
**Step 3:** 
[<button class="btn"><i class="far fa-arrow-alt-circle-right"></i>Integrating Shopify Apps</button>](/xstore/apps.html)


## Changing The Theme Styles
Since the concept of "theme styles" in the Xstore theme is the Demo Stores - which means you choose the style you want when you download the theme, there is no "change theme style" button within the theme editor. 
Instead, if you're looking to change your theme to use a different Demo Shop/ theme style, there are two options:  

**1. Use the Demo Shop import feature of Arena Dashboard Shopify App**  
- The Demo Shop import feature (via the Arena Dashboard App) will allow you to quickly change your theme to use the colors, settings, sections, and content of one of the other Xstore Demo Shops.  
- To switch the Demo, simply repeat [Step 4](/xstore/themeinstall#step-4-install-the-theme) in the theme installation process.
- Please note that importing a Demo Shop will not overwrite any existing theme settings, sections and options.  
<span style="color:blue">*This will install a brand new theme to your Theme Library panel.*</span>
![new theme](/xstore/switching-demo-complete.png)  

**2. Upload an alternate theme zip file from the 'unzip-me-first.zip' file**  
- When you purchased the Xstore theme, the theme downloads as a zip file called 'unzip-me-first.zip'. Once the file is unzipped, a folder called `Welcome to Flex` gets created.  
- There will be a main xstore.zip file, as well as zip files named after the current Demo Shops.  
- If you want to start off with a different Demo Shop as your theme style, you can upload your  Demo Shop zip of choice to Shopify. The theme will install with all of the sections, content, and settings as seen in that Demo Shop.   
<span style="color:blue">*This will install a brand new theme to your Theme Library panel*.</span>
![new theme](/xstore/switching-demo-complete.png)  

:::tip Inforcing the change
To make the change becomes permanent, you will need to Publish the theme you want to switch to. This can easily be done by following [Step 5](/xstore/themeinstall#step-5-publish-theme) of the installation process.  
After that you can find your appointed theme ready to be customized in the `Themes` tab of the `Online Store` section at your myShopify Admin Homepage.
![](after-publish-multiple.png) 
:::  