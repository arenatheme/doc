---
metaTitle: XStore - Theme Installation - ArenaCommerce Help Center
sidebarDepth: 2
---

# XStore Theme Installation

Let's begin the first step of building your dream online store!  
This step-by-step guide will show you completely how to install and verify your purchased theme.  
**<span style="color:red">[Google Chrome Browser](https://www.google.com/chrome/) is a requirement for installing and operating the theme and all its features.</span>**  

***For a complete visual tutorial, please view the video below:***
@[youtube](0AoE_3dLHjY)  

## Step 1: Installing Required App and Extension 
In order to install the XStore theme or the other themes in our store, you will need to install 3 essential app and extensions. They also play a pivotal role in helping you navigate and customize our theme swiftly and effectively. They are:
- Arena Shopify Admin Extension
- Arena Dashboard App
- Arena Dashboard Importer Extension  

To install them, please follow the step-by-step instruction below.  
### Installing Arena Shopify Admin Chrome Extension  
- Arena Admin Chrome Extension support to import our Demo Store Menu (Navigation) to your store. To enable it:  
  1. Go to [<button class="btn"><i class="far fa-arrow-alt-circle-right"></i>Arena Shopify Admin Extension</button>](https://chrome.google.com/webstore/detail/arena-shopify-admin-exten/acjopnffmehbackaohjjnpnnnocfcmnf).    
  2. Click `Add to Chrome` to install this extension.
  3. Click `Add extension`.
  4. Arena Shopify Admin Extension is now ready to work with the to-be-installed Arena Dashboard Importer.
![](/extension/arena-chrome-extension.gif)  

### Installing Arena Dashboard Shopify App
  1. Go to [<button class="btn"><i class="far fa-arrow-alt-circle-right"></i>Arena Dashboard App</button>](https://www.arenacommerce.com/products/arena-dashboard).
  2. Click **Install** Button.
  3. Input `Your Shopify Store URL`, click `Login`.  
    Shopify Store URL form are usually: `[examplestore].myshopify.com`.  
    ![](/install_dashboard_1.png)
  4. `Sign in` to your Shopify account.
  5. Click `Install unlisted app`.
  6. Use the app in the following step.  

### Installing Arena Dashboard Importer Chrome Extension
Arena Dashboard Importer helps in installing our theme to your Shopify store. To enable it:   
1. Go to [<button class="btn"><i class="far fa-arrow-alt-circle-right"></i>Arena Dashboard Importer</button>](https://chrome.google.com/webstore/detail/arena-dashboard-importer/noleolmfjimepebmefepgjcdojopafnn/)  
2. Click `Add to Chrome` to install this extension.
3. Click `Add extension`.
4. Arena Dashboard Importer is now ready to install your theme.  


## Step 2: Adding Your License Code
  **1. From your myShopify Admin Homepage, go to [Apps](https://www.shopify.com/admin/apps), click `Arena Dashboard`.**
![Dashboard App](/xstore/1arena-dashboard-app.png)
  **2. Click `Add Your License Key`.**
![Arena Dashboard](/xstore/2arena-dashboard-homep.png)
  **3. Insert the purchase code and click `Active License Key`. You can copy and paste the code from the License certificate & purchase code you downloaded through ThemeForest (Envato).**  

<details>
<summary> To find your purchase code, please expand this line and do as follows:</summary>  

- Log into your ThemeForest (Envato) account and hover above your username in the top right corner to access the dropdown list. Select `Downloads`.  
- Find the XStore theme in the list of item(s) you have bought.  
You can also find the link to the XStore theme [here](https://themeforest.net/item/xstore-premium-multipurpose-shopify-theme/26492260).  
- Click the `Download` button next to the theme.  
From the drop down menu, click ‘License certificate & purchase code’ (available as PDF or text file). It is better to download the text file so you can copy and paste the purchase code when adding the license key to Shopify. Open the file and you will find the purchase code.  
<div class="video-wrapper">
<iframe class="wistia_embed" src="//fast.wistia.net/embed/iframe/gqsrs2assi" name="wistia_embed" width="640" height="455" frameborder="0" allowfullscreen="allowfullscreen" sandbox="allow-scripts allow-forms allow-same-origin allow-presentation"></iframe>
</div>
Here is an example of a PDF License certificate and purchase code:
![](https://help.market.envato.com/hc/en-us/article_attachments/201801083/License_Purchase_Code_Example.png)
[Source](https://arenathemes.freshdesk.com/a/solutions/articles/6000116407-how-to-find-your-themeforest-item-purchase-code)

</details>

- Or click [here](http://help.arenacommerce.com/xstore/purchase-code.html) to view in another window.  

- After your license code was added, a confirmation banner will appear:
![](/xstore/purchase-code-confirmation.png)  

**4. Your purchased theme will now show in the 'Licensed' board.**
![Arena Dashboard](/xstore/6license-confirm.png)  

## Step 3: In order to add more theme License Keys, repeat Step 2.

## Step 4: Install the theme
To begin working on your store, you will need to unapck and upload the theme pack.  
To do that, you can install the theme via the Theme Installation Wizard by doing the following:

**1. From the `Arena Dashboard` app admin, click the second tab `Theme Installation`.**   
![Theme Installation Tab](/xstore/theme-install.png)  
You can also click on the `Theme Installation` button on the XStore theme tab in the License board. 
![Theme Installation Tab](/xstore/theme-install2.png)  

**2. Then when the **'Installing New Theme'** screen appear, click `Setup Wizard`.**
![](/xstore/setup-wizard.png)  

**3. Select Theme package & click `Next`.**  
![](/xstore/next.png)  

**4. When the 'Select Preset Demo Style' box appear, select `Version` and `Preset`.**  
You can choose from XStore's 68 presets ranging from Fashion, Sport and Beauty to Medical, Law and Corporation, the posibilities are endless!  
Please take a look at our list of presets, or Demos [here](https://landing.arenacommerce.com/xstore/). Preview of each preset will also appear on the left of the box with each preset you click on to help you better grasp what each Demo offers.
![](/xstore/theme-presets.png)  
- Click `Install Preset`. The app will install this theme package to your store right away!  
For this tutorial I will install the preset **'default'** of the version **'1.0-fashion'** as seen below.
![](/xstore/install-preset.png)  
:::tip
While there are options to skip installing the Collection and Product metafields, this will also prevent you to experience what the full extent our product can offer. However, if you are familiar with us and our product, you can choose these options to drive the installation process faster.
![/skip-collection-product](/skip-collection-product.png)
***Please be advise: once the installation process is done, if you clicked these two `Skip` options, you can never experience them in the Demo. The only way to get them is to reinstall the theme.***
:::  
<span style="color:purple">**If you have already has your own materials, click `Skip` to pass the import Theme Package step. This will stop import new theme package process to your store.**</span>  

**5. Check the Content Component you would like to import & click `Import`.**  
- Installation and activation of the Arena Dashboard Importer Extension is essential to import the Demo theme. If you had not install and activate the extension, you can click on the `Install` button to add the extension to your browser.  
***<span style="color:red">Please be aware that this is a Chrome-only extension and it is advisable that you install the theme and import the Demo on Google Chrome.</span>***
    ![](/xstore/theme-package-install-tasks.png)  
    - Wait for tasks to finish.
    ![](/xstore/task-running.png)
    - While importing, a dialogue box will appear and request to open Navigation. Click `OK`.
    ![request open](/request-open.png)  
    - A new window displaying Navigation section will appear. Click `Arena Menu Import`.  
    ![](/xstore/navigation-menu-request.png)
    - Wait for the task to finish and close the window.

  :::danger ***If error were to occur during import process, there are 2 possibilities:***
  1. You may not have installed and activated Arena Dashboard Importer Extension on Chrome.
  2. This is a result of a duplication in materials due to a previous unsuccessful theme install attempt or simply duplicate materials between installations.
  :::  

***For the first posibility, the error screen will be as followed:***
    ![](/xstore/error-need-dashboard-importer.png)
  - **When import task finish in this manner, just click `Skip`.**  
  - Make sure you have installed and activated Arena Dashboard Importer Extension on Chrome. After that, when you click `Skip`, the **'Uploading sample files & images to your website'** dialogue box will appear. Please go to the next step to continue.  

***For the second posibility, the error screen will be as followed:***
![](/error2.png)  
- **When import task finish in this manner, just click `Next`.** The ‘Uploading sample files & images to your website’ dialogue box will appear. Please go to the next step to continue.  

**6. Upload sample images and files to your store**  
![](/xstore/import-download-images.png)
  - Click **Download images**.
  ![download-image-pack](/download-image-pack.png)
  - Extract / Unzip the files. If you don't know how to extract, please follow the link below for further details.  

    ::: right
    Read more: [How to extract file](https://www.wikihow.tech/Extract-Zip-Files).
    :::  

  - Go to [Files](https://www.shopify.com/admin/settings/files) in the Settings button locate at the lower left corner of the myShopify Admin Homepage.  
    ![](/xstore/settings-locale.png)
  - Upload all files in the folder you have extracted. Then close the window.  
    ::: tip How to upload files on to Shopify:
    - From your Shopify admin, go to `Settings` > `Files`.
    - Select all the files in the folder you have just extracted.
    - Click `Upload files`.
    - Select one or more files to upload. Click `Open`.  
    :::  

**7. Integrating Shopify App into XStore theme**  
- You can choose to Install all the suggested apps into your Shopify at this point by clicking the `Install` button on each app, or finish the theme installation process here by clicking the `Finish` button.
![](/xstore/finish.png)  
- The Integrating Shopify App details will be covered in the next article. You can access it by clicking [here](/xstore/apps.md) or scroll to the end of this page and click **`Integrating Shopify App`** at the lower right corner.  


## Step 5: Publishing Theme
- Once the installation is finished, you can publish the theme to see first hand how it will play out on the web.  
If you have installed the Demo Shop, the theme will now be lively with demo images to help you understand further the functions of each section.  
- If you have installed multiple XStore Demo Shops, the theme you choose to publish will be the one customers actually see when your store goes live.  
- To publish a theme, simply go to the Arena Dashboard app in the `App` section of your myShopify Admin Homepage > choose the `Theme Installation` tab > scroll down to the list of `Installed Themes` > Click `More actions...` button on the theme you want to publish > Click `Publish`.  
![Publish theme](/xstore/theme-publish.png)
- After publishing the theme, you can view the theme in action on your domain *[your chosen name].myshopify.com*
![](/xstore/demo-preview.png)  

In the next step, we will go through the installation and integration of useful Shopify apps that will help boost customer's engagement and ease their buying process. You can access it by clicking [here](/xstore/apps.md) or click **`Integrating Shopify App`** at the lower right corner.  