---
metaTitle: XStore - Products Configuration - ArenaCommerce Help Center
sidebarDepth: 2
---
# Xstore Product Configuration

## How to Configure Product Section
@[youtube](BdP-06qS10E)
  
## Product Template & Duplicate Product Template
@[youtube](-NEtD3kk6VA)  

## Configuring Product Cards  
@[youtube](pJXFts2E9Xw)  

# Create a Product

The product page showcases a product and its variants, and includes product images, pricing information, a product description, and an Add to cart button. Every product that you have specified in the admin to be available on your online store has its own product page on your website. There are many ways that you can customize the layout, style, and behavior of your product pages.

For more detail about product page you may visit [https://help.shopify.com/en/manual/products/understanding-products](https://help.shopify.com/en/manual/products/understanding-products)

## Creating Product

[<button class = "markdown-button" name="button">PRODUCT SHOPIFY OFFICIAL DOCUMENT</button>](https://help.shopify.com/en/manual/products)

### Adding Product Variants

[<button class = "markdown-button" name="button">ADDING VARIANT OFFICIAL DOCUMENT</button>](https://help.shopify.com/manual/products/variants)

### Product Alternate Templates
[<button class = "markdown-button" name="button"> ALTERNATE TEMPLATE SHOPIFY OFFICIAL DOCUMENT</button>](https://help.shopify.com/en/themes/customization/store/create-alternate-templates)


### Configure Product Templates Layout by Section

:::theo
Shopify support only **1 product page template** to Section configure. In order to change to alternate Product templates:
In Theme Editor, **find and go directly to the product you already selected Product alternate template when working with Theme Editor**
:::

1. From your Shopify admin, go to `Online Store` > `Themes`
2. Find the theme click `Customize`
3. From the top bar drop-down menu, select `Product pages`.
4. In the `preview of your store on the right side of the page`, navigate to your product that uses the `product.alternate` page template.
5. Open the `Product (alternate)` pages settings.
6. Click `Save`.