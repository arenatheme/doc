---
metaTitle: XStore - Pages Configuration - ArenaCommerce Help Center
sidebarDepth: 2
---
# Xstore Pages Configuration  
With page creation covered in the general theme user guide [here](/customization/page.md), in this chapter, we will show you how to configure XStore general built-in page features.  

## 

