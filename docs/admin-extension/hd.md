---
metaTitle: Handy 4.0 Shopify Theme User Guide - ArenaCommerce
---
# Guide
## Heading
![](/extension/install.png)



- **list**
- 0st

::: warning
tip
:::

Tables        | Are           | Cool  
:-:|:-|------
col 3 is      | right-aligned | $1600 
col 2 is      | centered      |   $12 
zebra stripes | are neat      |    $1 

[<button class="btn btn-video"> <i class="fab fa-youtube"></i>Arena Dashboard Video Guide</button>](https://www.youtube.com/watch?v=0AoE_3dLHjY)

[<button class="btn"><i class="far fa-arrow-alt-circle-right"></i> Arena Shopify Admin Extension</button>](https://chrome.google.com/webstore/detail/arena-shopify-admin-exten/acjopnffmehbackaohjjnpnnnocfcmnf?hl=en).

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Installing Arena Wishlist & Compare App" id="wishlist-compare-settings"
Read more: [Install Arena Wishlist Compare app](/installation/apps.md)
:::
It is our free Shopify app to help you create a Wishlist by logging into your store account and simply start saving your favorites for later viewing.

::: tab "Arena Wishlist Compare Settings" id="wishlist-setting"
Wishlist & Compare is not offered by Shopify as a function so we have built a Free Shopify Application to assist customers.
- **Wishlist** is build by using Shopify App Proxy, it's not required to setup template page on your Shopify store. Wishlist function will launch at: https://your-shopify-url/apps/wishlist
- Comparing requires create and assign a separate page for the Compare page display
:::
::: tab "Theme Settings" id="wishlist-theme-settings"
- [x] Enable Wishlist
- [x] Enable Compare
:::
::::
