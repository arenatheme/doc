# Privacy Policy

## When installing Arena Shopify Admin Extension for Chrome, you might see the following prompt:
::: danger
Install Arena Shopify Extension Extension?
- It can access:
Your tabs and browsing activity
:::
## What type of data does the extension collects?
None, your data is yours and we do not want it. The extension does not collect any data. It never communicates in any form over the network.

# Automated Statistics of Use of the Extension

We may collect anonymised automated statistics of the use of our Extension on a specific computer. These statistics include datas such as the URLs visited, the usage frequency of each Extension parameter, and other user activities within the Extension. We use third party service Google Analytics to collect the statistics. We do not control how they handle the data being processed, so please familiarize yourself with Google’s Privacy Policy at https://www.google.com/intl/en/policies/privacy/ before you start using the Extension.    .

You can disable collection of the statistics in the “Settings” section of the toolbar. In this case, no abovementioned statistical data from your computer will be obtained by us.

## What type of data does the extension link to?  

One of the Arena Dashboard app functions is to support import navigation into Shopify Store ([myShopify username].myshopify.com). As a result, Arena Shopify Admin Extension can request Arena Dashboard Application to get data at: https://app.arenacommerce.com

## What data does the extension store?
Any option(s) you may set in the Settings tab are saved, so that they don't get lost when you close and re-open your browser.

## How can I be informed of any changes to the privacy policy?
Since we don't collect any of your information, we also cannot notify you of changes to this privacy policy.