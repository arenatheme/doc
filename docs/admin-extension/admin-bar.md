# Admin Quick Links function
## Introduce
 ***Admin Quick Links*** function is one of the functions of **Arena Dashboard extension**. This function will help you gain easier access to another editor page in Shopify as it gives you a board of shortcuts, in which each shortcut will lead you to an editor page that you will use regularly. Thus, help you save a lot of time!
   ![](/extension/Quick_Links.png) 

## Enable/Disable
 To enable/disable Admin Quick Link function, please follow the steps below:  
 `Find Arena Dashboard icon in Chrome Extension bar` 
 ![](/extension/arena_icon.png)> 
 `Click to open the icon` > `Choose Admin Quick Links tab` > `Check the box to enable Admin Quick Links and uncheck to disable` ![](/extension/Admin_QL.png)  
 -> `Refresh the current page` 
 
 After enabling ***Admin Quick Links***, a small box will appear at the top right of the screen.  
 When you hover the mouse over the box, the ellipsis icon will appear above the purple box with a cogwheel.  
  - To move the box around, you should `click and hold` the ellipsis icon and drag the box to where you see fit.  
  - To get to the functions, `click on the cogwheel icon` to expand the shortcut panel. There will be 9 icons for 6 shortcuts inside.
 ![](/extension/shortcut_box.png)

 ## Shortcuts panel
  There are 9 shortcuts we offer, divided in to two parts:
  ![](/extension/Panel.png)
  - Customize Theme & Content Editing: These shorcuts will lead you to editor pages, where you can customize your theme looks, config layout, add tabs, edit main menu,...
  - Store settings: These shorcuts will lead you to store pages, where you can add or edit data of your product, customer or manage apps.
### Customize Theme & Content Editing :
   1. Section Content -> Editor Page : Custom theme layout.
   2. Edit Content -> Current Product Page : Config data of specific product.
   3. Edit Custom Field -> Advanced Custom Field Page : Customize deep-in feature through Metafield system.
### Store settings :
   1. General: Store main settings.
   2. Customer: Customer managing page.
   3. Discount: Discount editor.
   4. Installed Apps: Manage apps.
   5. Menu: Nagavigation menu.
   6. Product: All products data.