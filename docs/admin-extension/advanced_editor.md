# Advanced Theme Editor
This function contains six additional tools for you to improve the way you edit theme contents.
![](/extension/ATE.png)

To enable/disable tool function:
 `Find Arena Dashboard icon in Chrome Extension bar` > `Click to open the icon` > `Choose Advanced Theme Editor tab` > `Check the box next to the tool that you want enable and uncheck to disable` > `Refresh the current page` 

 ## Icon picker
   When you click on an icon input box, an UI will appear for you to choose the icon that you want.
   ![](/extension/Iconpicker.png)
   ::: warning
   Remember to switch to the theme you are using to access with compatible icon.
   :::
 ## Date picker
   When you click on a date/time input box, a calendar UI will appear for you to pick the date.
   ![](/extension/Datepicker.png)
 ## Header collapsed
   This tool will automatically group minor tabs together at the beginning, thus make it easier for you to find the section you want to access.
   ![](/extension/Grmenu.png)
 ## Responsive spacing settings
   This will help you edit padding and margin size of the pop-up FOMO by pixel.
   ![](/extension/Spacing.png)
 ## Responsive carousel column settings
   This will edit the number of columns in gallery slider across different devices.
   ![](/extension/Columngallery.png)
 ## Advanced Text Editor
   To trigger the Advanced Text Editor, double click on any text input box, an UI will pop-up to help you change text style.
   ![](/extension/Advtext.png)