---
metaTitle: Arena Dashboard Chrome extension app
---
# About

Arena Shopify Admin Extension lets you replace the default Shopify Theme Editor dashboard with new UI to easily manage the Shopify Theme.

## Main functions
- **Admin Quick Links**: This function helps you gain easier access to another editor page in Shopify as it gives you a board of shortcuts, in which each shortcut will lead you to an editor page that you will use regularly. 
- **Advanced Theme Editor**: This function extends your Shopify Theme Editor, gives you six additional tools to improve the way you configure Shopify theme.
- **Content Editor**: This function allows you to easily add columns and buttons into your Shopify contents (products, pages, blog) with a click of a button .
- **URL Filter**: This function helps you choose which domain the Extention will run in.
- **Import Menus**: This funtion imports menus from Arena Dashboard into your Shopify store.

## Install
[<button class="btn"><i class="far fa-arrow-alt-circle-right"></i> Arena Shopify Admin Extension</button>](https://help.arenacommerce.com/installation/dashboard.html)
## Note 
  - At the moment, **Arena Dashboard Chrome extension app** only supports Shopify store.
  - **Arena Dashboard Chrome extension app** is still under further development.  
  If you have any feedback and query, please let us know at: support@arenathemes.com.
  - To make your experience with our extension easier, please read the instructions carefully.


## Instruction Video

You can watch the instruction details in this video of how to use **Arena Dashboard Chrome extension app** here: 

[<button class="btn btn-video"> <i class="fab fa-youtube"></i>Arena Dashboard Video Guide</button>](https://www.youtube.com/watch?v=RmoDk9V7bg0)



