# Content Editor
  ## Introducing
   ***Content Editor***  allows you to easily add columns and buttons into your Shopify contents (products, pages, blog) with a click of a button.
  ## Enable/Disable
  To enable/disable Admin Quick Link function:
  `Find Arena Dashboard icon' in Chrome's extensions bar` > `Click to open the icon` > `Choose Content Editor tab` > `Check the box to enable Content Editor and uncheck to disable` > `Refresh the current page` 
  
  ::: tip
  Content editor will only appear when you are working with admin theme pages.
  :::
  
  After enable ***Content Editor***, a small box will appear at top right of the screen, when you hover you mouse to the box, it will extend to "Add box" at the bottom and "Ellipsis icon" at the top. You can `Click and hold` the "Ellipsis icon" to move the whole box around. Click "Adds box" to open the column form panel.
  ## How to make columns
  To use this function, you have to switch to code mode in the description section by clicking "<>" icon on top right corner of the description box.
  ![](/extension/Content_Edit1.png)
   After that, open the column form panel, click the column form that you need, a small red box will appear in front of the panel.
  ![](/extension/Copied.png)
  Now click in code mode in description section, choose position that you want to make column text and the press `Cmd/Ctrl` + `V`
  A code form look like this will appear:

```html
<div class="row"> <div class="col-md-6 col-12">Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien</div> <div class="col-md-6 col-12">Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien</div> </div>
```
  Replace "tiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien" part by the content you want to insert.
  
  Upon finishing, save the process and refresh the theme page.