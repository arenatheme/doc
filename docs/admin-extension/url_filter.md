# URL Filter
This function will make a whitelist of domains you want the extension to appear exclusively, and a blacklist of domains you want the extension to never appear.
![](../.vuepress/public/extension/urlfilter.png)
With both lists, you just have to type in a small part of the URL, for example:
    If you type "admin" in whitelist, the extension will only pop-up at Shopify URLs containing the "admin" element.