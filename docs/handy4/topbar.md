---
metaTitle: Handy 4.0 Shopify Theme Top Bar User Guide
sidebarDepth: 1
---
# Handy Top Bar

## Using the Top Bar (aka Announcement Bar)
The 'Top Bar' feature in the Handy theme is the perfect way to draw attention to any promotions, sales, link detail or important information that you want your customers to know about!

Your announcement will appear the header throughout your site – or just on your homepage.

The 'Top Bar' feature includes options to add text, image, an icon and button. You can also configure the text and background colors, layout.

**In order to disable** the Top Bar Section, head into **Customize (your Theme Editor) > Theme Settings > Top Bar > Disable Section.**

Please remember that when you disable the Top bar section, it won't show in you theme Sections Editor `(Customize > Sections)`.

## Topbar Sections Layout & Style Settings

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Top Bar Content" id="handy-top-bar-content"
### Topbar Sections Layout & Content
Head into **`Customize` > `Sections` > `Topbar`** you'll see the following settings available:
:::
::: tab "Top Bar Setting Styles" id="handy-top-bar-color"
### Topbar Style Settings
Head into **`Customize` > `Theme Setings` > `Topbar`** you'll see the following settings available:
:::
::::