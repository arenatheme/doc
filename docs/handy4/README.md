---
metaTitle: Handy 4.0 Shopify Theme User Guide - ArenaCommerce
sidebarDepth: 0
---
# Handy 4: Theme Features

**Handy 4.0** Shopify theme is a great start for any one who is looking to start his one handmade online shop or looking to create a handmade goods marketplace like Etsy or Amazon.
It's is the most **versatile, intuitive, and easy to use multi-purpose** Shopify theme on the market.
Below we have highlighted some of Handy features! The list may be long, but so are the reasons to buy Avada and join the most significant Shopify theme community out there! 
### Amazing Demos Importer with Arena Dashboard
No other theme has this. The Best Demo Importer On The Market – Industry leading demo import that is amazingly easy to use and the fastest way to build your website. One click demo import allows you to install a full demo with everything, or a partial demo. Want the Creative demo but with Modern Shop products for an eCommerce site? 
### Advanced Theme Options
- Full control over the entire layout; site width, content area, sidebars, product tabs and more
- Extensive options which provide incredible customization options without having to modify code
- Lazy loading Image: Load images super fast with build in lazy loading and adaptive image. Show the optimized image size for any screen size, only when needed.
- Full Page Sections: Homepage, Collection, Blog, Cart...
- Social Icons and Theme Icons are CSS Font Icons, no Images: Arena Font Icons with Visual Icon Selection to make things easiers for you.

### Advanced Typography Options!

- Strong focus on typography, usability and overall user-experience
- Advantage Typography with Shopify Fonts & Google Fonts.
- [Multiple Header Designs](/electro5/header.md)
- Unlimited Layout Collections

### Shopify App Integration

### Advanced Product Layout Options
- [Prefine Product Templates](/electro5/product-templates.html#product-templates)
    - Product Simple
    - Product Extended
    - Product Bundle
    - Product Redirect
### Custom Page Templates & Page Options Included
- Prefine Page Templates with specific block layout
    - [Store Location](/electro5/page-templates.md#store-location-page)
    - Store Location Build in https://handy.arenacommerce.com/pages/store-locator
    - About
    - Contact
    - 2 Different Product Vendor List 
### Wide & Boxed Layout Versions
Full control over site width; content area and sidebars

### Advanced Header Options
- Flexible Header Positions
- Sticky Header Setup & Options
- Unlimited Mega Menu
- Mobile Menu
### Advantaced Mega Menu
- Create beautiful menu layouts, Mega or Flyout Submenus
- Responsive & Mobile Optimized
- Tabbed Submenus
- Custom Content & Widgets
#### Advanced Search Options
- Build-in **Live Ajax Search**
### Advanced Footer Options
- Flexible Content Layout: Top/Bottom Footer
- Add Content: Logo, Information, social icon, payment icons, menu, ...
### Advanced Shop Options
- Newsletter Popup - support Mailpchimp & Klaviyo
- Build-in Wishlist & Compare by Free Arena Wishlist & Compare Shopify app
- Build-in Quick View
- Catalog Mode
- 2 Different Product Vendor List 

#### Handy 4.0 is Multi-Lingual/RTL Ready!
- Build-in Multi Currency Support
- EU Cookies Policy Notify - GDPR Compatibility

### Always up to Date
