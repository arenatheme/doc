# Blogs

## Blog Page Configuration

This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Handy 4.0 Themes` > `Customize` > `Shopify menu dropdown` > `Blog posts` > `Blog pages`

![](/electro5/blog_section.png)
![](/electro5/blog_categories.png)

## Blog Article Configuration

Refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Handy 4.0 Themes` > `Customize` > `Shopify menu dropdown` > `Blog posts` > `Article pages`

![](/electro5/blog_articles.png)
