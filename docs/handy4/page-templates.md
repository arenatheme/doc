# Handy 4 Pages
::: warning
Before configuare the following page, you must **Publish** the Shopify theme
:::

## Page Templates
Page Template Name|Template Description|Quantinty Limit
:-|:-|:-
page|default page|infinity
page.about|About us page template|1
page.contact|Contact us page template|1
page.store.locator|An interactive store locator with real time driving directions page without app|1
page.bc_compare|Page required by Arena Wishlist & Compare | 1
page.bc_wishlist|Page required by Arena Wishlist & Compare | 1


## About us Page

1. [Create new page](../customization/page.md#how-to-create-a-shopify-page)
2. Select template "page.about"
3. Save page
4. Customize Page Content by Section

### Customize Page Content
Ensure you have an article with page.about template (or blog post) <strong>Published</strong>.

![](/electro5/page_about_us.png)

- From your Shopify admin, to the `Online Store` > `Themes` page and use the Customize theme button to start customizing your theme.
- From the top bar drop-down menu, select `About us` page to configure.
- Open the Page settings.

About us support 5 blocks content type:

* Banner
* Image & Text
* Team member (Our team)
* Text box

## Contact Us Page
1. [Create new page](../customization/page.md#how-to-create-a-shopify-page)
2. Select template "page.contact"
3. Save page
4. Customize Page Content by Section


## Store Location Page
1. [Create new page](../customization/page.md#how-to-create-a-shopify-page)
2. Select template "page.contact"
3. Save page
4. Customize Page Content by Section

### Configure Store Location Page
![](/electro5/page_store_location.png)

- [ ] Set Google Maps API 
::: right
[How to get Google Map API](https://arenathemes.freshdesk.com/support/solutions/articles/6000209417-creating-a-google-maps-api-key)
:::
   
- [x] Geolocation service: Select it to use location service to support customer find the store Near by.  
- [ ] Configure store locator: *Configure your main store location to find near by store when customer don't accept to use Geolocation services.*  
Format: `Latitude, Longtitude`
::: right
[Get your Store Latitude and Longitude](https://www.latlong.net/)
:::


### Add Store Location

![](/electro5/store_location.png)
