---
metaTitle: Handy 4.0 Shopify Theme Header & Navigation
sidebarDepth: 3
---
# Handy Header & Navigation


## Using the Header Section
The Handy theme includes a highly customizable 'Header' section. The Header has 6 layout styles on Desktop & 3 layout styles on Mobile device and includes many built-in settings, including different alignment options for your main menu, enabling search, and the ability to add in your own custom CSS.
In order to configure your Header, click into **Customize** (your Theme Editor) > **Sections** > **Header**.

There are some other options that use in all Header style that you can config at **Theme Settings**
In order to configure your Header general settings, click into **Customize** (your Theme Editor) > **Theme settings** > **Header**.

## Header General Settings 
::: theo
`Online Stores` > `Themes` > `Handy 4.0 Themes` > `Customize` >  `Theme settings` > `Header`
:::


## Header Section Settings & Styles
::: theo
`Online Stores` > `Themes` > `Handy 4.0 Themes` > `Customize` >  `Sections` > `Header`
:::

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Header Style 1" id="header-1"
![](/handy4/header1.png)
:::
::: tab "Header Style 2" id="header-2"
![](/handy4/header2.png)
:::
::: tab "Header Style 3" id="header-3"
![](/handy4/header3.png)
:::
::: tab "Header Style 4" id="header-4"
![](/handy4/header4.png)
:::
::: tab "Header Style 5" id="header-5"
![](/handy4/header5.png)
:::
::: tab "Header Style 6" id="header-6"
![](/handy4/header6.png)
:::
::::

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Header Mobile Style 1" id="header-mobile-1"
![](/handy4/mobile_h1.png)
:::
::: tab "Header Mobile Style 2" id="header-mobile-2"
![](/handy4/mobile_h2.png)

:::
::: tab "Header Mobile Style 2" id="header-mobile-3"
![](/handy4/mobile_h3.png)

:::
::::

::: theo
`Online Stores` > `Themes` > `Handy 4.0 Themes` > `Customize` >  `Sections` > `Header`
:::

Header is a Static Section apply settings to all pages.

## Header Configuration


## Navigation Configuration

### Sticky Menu

![](header_sticky_desktop.png)
![](header_sticky_mobile.png)

### Menu Fly Out & Highlight
In default, the mega navigation will flyout at direction at Right side of the drop-down menu. However, if you would like to change the direction flyout, you may add the menu titles(label) into `MENU FLY OUT LEFT` field. Add a separator ; for each title

- Eg. Menu name is Flyout Left -> input `Flyout Left`
- Menu name is: Contact Us -> input `Contact Us`
- Input Flyout Left; Contact us if you would like to show Flyout Left & Contact us sub menu at the left side
![](header_fly.png)
### Menu Highlight
Options to highlight the menu item in Horizontal & Verticle Menu.
![](header_highlight.png)

