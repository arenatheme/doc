# Cart Section

::: theo
Refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Handy 4.0 Themes` > `Customize` >  `Sections` > `Cart`
:::

![Electro 5 Collection Cart Page](/electro5/cart_section.png)

## Cart Configuration

![Electro 5 Collection Cart Page](/electro5/cart_section_1.png)
