---
metaTitle: Handy 4.0 Shopify Theme General Settings
sidebarDepth: 2
---
# Handy 4: Theme Settings

This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Handy 4.0` > `Customize` > `Theme Settings`

