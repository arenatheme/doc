---
metaTitle: Configure Header Section Shopify Theme Handy 4.0
---

# Footer Section

::: theo
Refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Handy 4.0 Themes` > `Customize` >  `Sections` > `Footer`
:::
## Configuration
![](/electro5/footer_section.png)

## Content
Combinate cotent and you will get more and more footer designs. Please remember to check your total column width settings must equal 12.

![](/electro5/footer_content.png)

