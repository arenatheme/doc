---
metaTitle: Handy 4.0 Shopify Theme App Integration
sidebarDepth: 1
---

# Handy App Intergration
## 1. Apps Intergration

Step 1: [<button class="btn">Installing Shopify Apps</button>](./installation/app.md)  

Step 2: Configure Apps & Theme Setting
```
Online Stores > Themes > Handy 4.0 > Customize > Theme Settings > Apps Intergration
```

### WISHLIST & COMPARE

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Installing Arena Wishlist & Compare App" id="wishlist-compare-settings"
Read more: [Install Arena Wishlist Compare app](/installation/apps.md)
:::
It's our free Shopify app to support Customers can create a Wishlist by logging into their store account and simply start saving their favorites to it for later viewing.

::: tab "Arena Wishlist Compare Settings" id="wishlist-setting"
Wishlist & Compare is not offered by Shopify as a function so we have built a Free Shopify Application to assist customers.
- **Wishlist** is build by using Shopify App Proxy, it's not required to setup template page on your Shopify store. Wishlist function will launch at: https://your-shopify-url/apps/wishlist
- Compare required create and assign a separate page for the Compare page display
:::
::: tab "Theme Settings" id="wishlist-theme-settings"
- [x] Enable Wishlist
- [x] Enable Compare
:::
::::

::: right
Read more: [Install Arena Wishlist Compare app](/installation/apps.md)
:::

### TRANSLATE LANGUAGES

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Theme Settings" id="theme-settings"
- [ ] Enable language translator: translate the website into other languages.
- [ ] Translation Method: Google Language / Weglot
- [ ] Original language: default language
- [ ] Destination languages: Choose languages you want to translate into. Supported languages can be found [here](https://weglot.com/documentation/available-languages).
- [ ] Make Private: Only work in Dev mode
- [ ] Auto Switch: Cache switch Language
- [ ] API Key Weglot: Your Weglot API key.
- [ ] Flag Type: Flag sharp in Header

:::
::: tab "Weglot Register & Settings" id="weglot-setting"
#### Weglot Free?
After signing up, you have a  10-days free trial period to test Weglot. Once the 10 days free trial ends, you have 2 possibilities:
- Your website has less than 2000 words, and you want to translate it only in one language: You can use the Free plan. The Free plan is automatically chosen at the end of the trial period, you don't need to choose it manually.
- Your website has more than 2000 words: You will need to upgrade your plan to continue using Weglot.
You can find your number of translated words in your account, on your dashboard.
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/5d0a37de2c7d3a1cad5b8f13/file-VIzht1Oy3D.png)
Detailed pricing:
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/5d0a389b2c7d3a1cad5b8f21/file-uJgnrENCUU.png)
#### Register Weglot with 20% Discount <Badge text="Discount 20%"/>

- From your Shopify admin, go to `Apps > Arena Dashboard > Perk`
![](https://cdn2.shopify.com/s/files/1/0208/3880/9664/files/dashboard_large.png?v=1563251258)
- Hover to Weglot & Click **Claim Perk** to get promotion code (discount 20%) & go to register website Weglot.
Please save Promo code & you can redeem it later when you want to upgrade the service.
- Enter `your email`, `password`,  check `I agree to Weglot's Terms of Service` and click **Start Free Trial**.
- Verify your email address and go to Weglot Dashboard.
- From your Weglot Dashboard, you can access the projects tab at the very top left. Once you click on it, you'll see a drop down with a green button "Create project" to add a new project (see below).From this tab, you can also select the project you want to work on.
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/59e620d22c7d3a752de12a7f/file-wWBkWK7Dom.png)
- Enter your Project name, select website technology is Shopify.
- Click Next to create Project
- You can find the different project settings detailed below on your Weglot account, under "project settings".
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/5d0a28022c7d3a1cad5b8e1a/file-B5tX8sHUdP.png)
    - **Name and website URL**
        Give a name to your project to easily navigate between your different projects. 
        ::: theo
        ![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/59e62b6d2c7d3a752de12afc/file-KURaNthtH6.png)
        In order to get the Visual Editor working properly, please make sure to add the correct website URL in the settings.
        :::
    - **API key**: A separate unique API is created for each project.
        ![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/59e62acc2c7d3a752de12af4/file-FzX2fu2bmz.png)


:::
::::
### PRODUCT REVIEW

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Theme Settings" id="theme-settings"
Product review by:

- [ ] None: Disable / Hide product review
- [ ] Product Review: Using Shopify Product Review app
- [ ] Judge.me: Using Judge.me app
:::
::: tab "Judge.me Settings" id="judge-setting"
#### Judge.me
It's require to select install Judge.me Review Widget Installation `Manually` because we already integration into the theme code.
![](/judgeme_manual.png)
:::
::::

