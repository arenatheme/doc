# General Settings

Handy includes numerous layout options for certain features of the theme. Within the theme's Layout Settings you can select your styles and templates to transform your shop and give it a unique look and feel!

``` tip
Online Stores > Themes > Handy 4.0 Themes > Customize > Theme Settings > General Settings
```  
These are the theme features that when chosen/enabled, will show up throughout your shop!

|| Parameter | Description
:-:|:-|:-
Enable lazy loading images || Lazy loading is technique that defers loading of non-critical resources at page load time. <br>Instead, these non-critical resources are loaded at the moment of need. <br>Where images are concerned, "non-critical" is often synonymous with "off-screen".

SHOP| Enable Catalog Mode | Disable Add to cart button
^^|![](/catalog_mode.png)||
^^|Display messages to remind free shipping offer| Display shipping threshold message
^^|Free shipping total cart require| Enter the number of total cart to free shipping condition</br>Currency: base on store setting
^^|![](/shipping_threshold.png)||
STATIC SECTIONS| Homepage Static Sections | Setting to enable/disable sections for homepage



### Announcement Bar - TopBar Sections
- [x] Announcement Bar: Enable Homepage Announcement Bar Static Section
![](/top_bar.png)
### Mobile Menu Icon
- [x] Mobile Department: Enable Mobile Menu with icon Section
![](/menu_mobile_icon_section.png)

### Mobile Bottom Bar

![](/inside_mobile.png)




