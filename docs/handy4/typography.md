---
metaTitle: Handy 4.x Shopify Theme Typography
sidebarDepth: 1
---
# Handy 4: Typography

Like other Shopify themes, the Handy theme will include typography settings within the Theme Editor (customize) under **Theme Settings > Typography**. These settings will apply theme wide to your body text, headings, sub-headings etc.

In addition the those, the Handy theme also includes a couple of **per section & theme setting part** typography settings. This means that in some of Handy's sections and features, you can update the typography for that specific area instead of the default typography settings getting applied.


## Typography Theme Settings
::: tip
If you head into **Online Stores > Themes > Handy 4.0 > Customize > Theme Settings** you'll see the following settings available:
:::
![](/handy4/typo.png)
### Header Fonts
Within the settings for the **theme's headings**, you can select a font using **Shopify's Font Picker** or **Google Fonts**, determine the **base size, font weight** for Heading 1 -6.
### Header Fonts
Within the settings for the **theme's headings**, you can select a font using **Shopify's Font Picker** or **Google Fonts**, determine the **base size, font weight** for Heading 1 -6.
The heading settings apply to: 
- any "heading" fields within sections (except for headings on banner images)
- headings within sidebars
- headings of footer columns
- heading within menus
- heading within Blog titles...

### Body Fonts
The body text applies to your paragraph text throughout the theme – this includes:

- product descriptions
- text entered into any 'text' fields within sections. 
- button labels
- announcement bar text
- breadcrumb links
- sort by/tag filter
- quick view button
- pagination
- icon bar
- product stickers
- product info for collection thumbnails
...
### Heading Section

- Heading Font Size; Select default Heading type for Heading Section. 
    If it's selected 3, your Section Heading default would be H3 tag and inherit the settings of H3 above...
- Sub Heading Font Size; Select default heading type for Sub Heading Section. 
    If it's selected 4, your subHeading default would be H4 tag and inherit the settings of H4 above.

### Text Transforms

Capitalization options following elements
- Breadcrumbs: Uppercase
- Buttons: Uppercase
- Navigation / Tabs : Uppercase
- Sections Heading: Uppercase
- Sidebar Heading: Uppercase


## Per Section & Theme Part Typography

If you're looking to have more control over the typography options in some of your sections, you're in luck! In some of Handy's section and features, you can update the typography for that specific area right within the section settings or theme settings part:

- Topbar (Theme Settings > Topbar)
- Navigation (Theme Settings > Navigation)
- Footer (Theme Settings > Footer)
- Product Card (Theme Settings > Product Card)
- Product Page (Theme Settings > Product Page)
- Homepage Sections (e.g. Sections > Slideshow > Image slide > Text box)
...