# Handy 4: Choosing a Demo Shop Style

### When to use a Handy demo shop as a starting point:

- You have a website in mind that you want to copy.
- You need to get your shop live ASAP!

### The main factors that you should consider when choosing a demo shop:
- Choose the demo that looks most like the layout you'd like your shop to have.
- Remember that you can **mix + match everything you see in the demos**, e.g. it's easy to keep a layout like Home 1 & Home2 but give it a color palette...

### Tips on choosing which demo shop to start with:


1. The main factors that you should consider when choosing a demo shop: 

If your industry is already represented by one of the shops, choose that one so that you'll be able to quickly populate your shop with relevant images/text (you can always change colors + layout but you'll find it most helpful to already have shop-appropriate content built right in from the beginning!)
- **Apparel/Fashion**
    * [Home 7](https://i.arenacommerce.com/go/handy/home-7clothes/)
    * [Home 8](https://i.arenacommerce.com/go/handy/home-8fashion/)
    * [Home 9](https://i.arenacommerce.com/go/handy/home-9fashion-2/)
- **Accessories**
    * [Home 12 - Boots](https://i.arenacommerce.com/go/handy/home-12boots/)
    * [Home 13 - Bags](https://i.arenacommerce.com/go/handy/home-13bags/)
    * [Home 14 - Sneaker](https://i.arenacommerce.com/go/handy/home-17sneaker/)
    * [Home 15 - Bag Store](https://i.arenacommerce.com/go/handy/home-13bags/)
    * [Home 16 - Jewelry](https://i.arenacommerce.com/go/handy/home-14jewelry/)

- **Bikini & Bras**
    * [Home 10](https://i.arenacommerce.com/go/handy/home10bra/)
    * [Home 11](https://i.arenacommerce.com/go/handy/home-11bra2/)
- **Handmade & Crafts**
