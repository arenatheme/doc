---
metaTitle: Configure Product Pages Shopify Theme Handy 4.0
sidebarDepth: 2
---
# Handy 4: Products

## How to Create a New Product Template in Shopify Theme

@[youtube](Hx8154ywLfE)
## How to add multiple tabs to Product pages

@[youtube](NkY9rM6t0FE)