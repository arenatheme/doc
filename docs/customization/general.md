---
metaTitle: User Guide General - ArenaCommerce Help Center
sidebarDepth: 3
---  
# Arena Theme User Guide General

Thank you for choosing our product! Starting an online store on one’s own can be the experience of a lifetime. We understand that it can be quite a handful familiarizing yourself with Shopify & our themes as there are a lot to learn, especially if you has no background in information technology and ecommerce.

In light of this, we have prepared a starter pack full of useful information and guidelines of intuitive help videos, articles and tutorials to help your transition easier. The User Guide is divided into 2 sections:  
- The General Section, as you can see occupying the first half of the Theme User Guide dropdown menu, is consist of this article and 10 essential items you will need to get familiar customizing to optimize your online business. Within these articles, only general information will be mentioned as the customization process may differ from theme to theme. The link to the in-depth information of each theme has been put in each article to ensure easy navigation.  
- The Theme Specific User Guide section occupying the lower half of the Theme User Guide dropdown menu, is consist of theme names and the link to their respecting User Guide. Please click on the word 'User Guide' under each theme's name to go to their respective comprehensive manual.  

This User Guide was created to help you quickly make sense of the configure and customization process of the theme and its functions. Please read it carefully, it will help you eliminate most potential problems and should you need to contact us or Shopify for support, you will have a deeper understanding of the the problem you encountered to save the precious time and money required for a thorough support session.  



