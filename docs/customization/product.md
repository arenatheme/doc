---
metaTitle: Customizing Products - ArenaCommerce Help Center
sidebarDepth: 2
---
# Customizing Products  

Since you are using Shopify platform, most of the configurations regarding Products, Collections, Metafields, etc... how-tos are already presented in the [Shopify Help Center](https://help.shopify.com/).  
In there, you will find the following tutorials, some of which our theme have added further customization tools which I will elaborate later in this page:  
- Adding and updating products  
- Product details  
- Variants  
- Product media  
- Collections  
- Selling gift cards  
- Importing and exporting products  
- Searching for products and filtering your product list  
- Managing inventory  
- Duties  
- Selling services or digital products  
- Metafields  
- Selling CBD on Shopify (US only)  
- Temporarily changing your product line

As for the Products tutorial, our friends at Shopify have made a very intuitive, in-depth tutorial [here](https://help.shopify.com/en/manual/products).  

For the Arena User Guide, we will go into details about how our themes customization tools help optimize Shopify's basic functions even further so you can better adapt to customers needs and increase sales conversion. Please refer to each theme's User Guide to find details about how to customize each function according to that specific theme. You can find each theme's Products setting guide below:  

---
- Electro 5.0 - 6.0: [Products Guide](/electro5/product-templates.md)
---  
- XStore 1.0+: [Products Guide](/xstore/product.md)
---  
- Handy 4.0+: [Products Guide](/handy4/product.md)  
---
- Zeexo 1.0+: [Products Guide](/zeexo/product.md)
---  