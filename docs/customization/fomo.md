---
metaTitle: Built-in Fomo Guide - ArenaCommerce Help Center
sidebarDepth: 2
---

# Free built-in Shopify Fomo  
::: tip Supported Versions
The details shared here apply to the most recent versions of our themes. To keep your theme up to date.
- Handy version 4.0+
- Xstore version 1.0+
- Bookshop version 2.0+
:::

## Page Rules

## Popup Refined Settings

## Content Filter 

**Bold**
- ['text' | bold]  
- ['text | bold 100]  

Bold values support from 100 to 900, and will modify that variable's value only.

**Colors**

- ['text' | color red]  
- ['text' | color #0000ff]  

Color codes only support 3 or 6 digit hexcodes, and will modify that variable's value only.



**Casing**  
Suppose your merge variable 'first_name' is "AreNa."
- ['AreNa' | propercase]
- ['AreNa' | upcase] -> ARENA
- ['AreNa | downcase] -> arena

**Animations**
- ['text' | tada!] This will give your notification a little "TADA!" as the notification displays.

## Geo-location Cities List  