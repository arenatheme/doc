---
metaTitle: Shopify Theme Customization - ArenaCommerce Help Center
sidebarDepth: 2
---

# Start Customizing Your Theme

Since you are using Shopify platform, most of the customization basics has been covered in the Shopify Official Documents here
[<button class="btn"><i class="far fa-arrow-alt-circle-right"></i>Shopify Customizing Theme Official Documentation</button>](https://help.shopify.com/en/manual/using-themes/change-the-layout).  

As a result, this User Guide will only contain Arena's theme customizations and the basic navigation, as well as steps to make these customs.

:::tip Before you start customizing your theme
It's a good idea to [Duplicate your theme](https://help.shopify.com/en/manual/using-themes/managing-themes/duplicating-themes) to create a backup copy. This makes it easy to discard your changes and start again if you need to.
Our friend at [Rewind](https://rewind.io/) also offer backup service for your theme in the long run. You can see more details [here](/xstore/quickstartguide.md#backup-your-store).
:::   

There are 2 ways to customize your theme, you will need to use them both in some situations to achieve your customization goals. One way is using Shopify's Theme Editor, the other is to dive in the theme's code.  

## Using Shopify's Theme Editor
You can customize your theme settings by using the Theme Editor. The theme editor includes a theme preview and a toolbar that you can use to add, remove content and make changes to your settings. To go to the Theme Editor:  
- From your **myShopify Admin Homepage**, go to **`Online Store` > `Themes`**.
- Find the theme that you want to edit and click **`Customize`**.  

### Edit your theme settings
The theme editor toolbar is divided into **`Sections`** and **`Theme settings`**.
![Theme Editor](https://help.shopify.com/assets/themes/editor-tabs-d6a578d45bf5feac7da3f618de523b00eacf724049c10e6cd6348fd061bb7e1f.png)  

* **`Theme settings`** is the backbone of a theme included settings default for your Shopify store at all pages.
You can edit your **theme settings** to customize your online store's **content, layout, typography, and colors**. Each theme provides settings that allow you to change the look and feel of your store without editing any code.

* You can use **`Sections`** to modify the content and layout of rach section on different pages of your store. The customizing option in each section is different based on the theme you chose to install. If you wish to see their manual and action, please go to the theme's corresponding User Guide.  
When you first open the **`Theme Editor`**, the settings for the **`Home page`** are shown. To access the settings for other pages, select the page type from the top bar drop-down menu as seen below:
![](https://help.shopify.com/assets/manual/customize/editor-product-e42f3cd79a23002e746a785271cc687537498560926798c5ccbff819050a2bf0.jpg)  


## Dive into the Theme Code
You can also edit the code for your theme, especially if you choose to incorporate third party apps to your store. ***Again, please make sure you have your theme and store backup first before considering these changes.*** We also highly recommend working with your app provider to incorporate the app into your store first before taking matters into your own hands to prevent any unsuspecting errors. We are also unable to modify our theme code to accommodate the app of your chosen. Please read our policy on third-party app [here](/quickstart/#_1-5-third-party-apps) before continue.  

To edit the theme code,
- From your **myShopify Admin Homepage**, go to **`Online Store` > `Themes`**.
- Click **`Actions` > `Edit code`**.
The code editor shows a directory of theme files on the left, and a space to view and edit the files on the right:
![](https://cdn.shopifycloud.com/help/assets/manual/customize/edit-html-css-page-example-446978943217ea9e34268bf85ee020c44470b8e4c3aa0e7aa9b797b1408cac17.png)

When you click a file in the directory on the left, it opens in the code editor. You can open and edit multiple files at once. Any files that you modify will show a purple dot next to the file name:
![](https://cdn.shopifycloud.com/help/assets/themes/editor/modified-files-36c1584d25bffc09d11fb4e0b006c5d5876f45f603fd6fa1b5c6b76e808875e8.png)

This can help you to keep track of where you have made changes.

### Reverting the theme code  
If you want to revert any changes to a file after you click **`Save`**, then open the file and click **`Older versions`**. A drop-down menu shows the date and time for each save that you've made. Select the version that you want to revert to, and click **`Save`**.  
