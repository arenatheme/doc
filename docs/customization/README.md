---
metaTitle: Arena Quick Start Guide - ArenaCommerce Help Center
sidebarDepth: 3
---

# Arena Quick Start Guide

Starting an online store on one's own can be the experience of a lifetime. We understand that it can be quite a handful familiarizing yourself with our themes as there are a lot to learn, especially if you has no background in information technology.  

In light of this, we have prepared a starter pack full of useful information and guidelines of intuitive help videos, articles and tutorials and pdf guide (coming soon!) for offline access. You will also see the links to articles containing frequently encountered problems along with some tips to help make the learning process easier and more enjoyable for you.  

The User Guide is divided into 2 sections:  
- The General Section, as you can see occupying the first half of the Theme User Guide dropdown menu, is consist of this article and 10 essential items you will need to get familiar customizing to optimize your online business. Within these articles, only general information will be mentioned as the customization process may differ from theme to theme. The link to the in-depth information of each theme has been put in each article to ensure easy navigation.  
- The Theme Specific User Guide section occupying the lower half of the Theme User Guide dropdown menu, is consist of theme names and the link to their respecting User Guide. Please click on the word 'User Guide' under each theme's name to go to their respective comprehensive manual.  

This User Guide was created to help you quickly make sense of the configure and customization process of the theme and its functions. Please read it carefully, it will help you eliminate most potential problems and should you need to contact us or Shopify for support, you will have a deeper understanding of the the problem you encountered to save the precious time and money required for a thorough support session.  

When you run into problem with our themes, never hestitate to use the live search function at the center top of the page, as most of the time the answers to your questions had been answered in one of our posts or videos.  

If you are unable to find an answer to your questions, please check our [**Before You Submit A Ticket**]() page first before submitting a support ticket [here](https://arenathemes.freshdesk.com/support/tickets/new) or clicking `Support` button in Arena Dashboard App you installed in your myShopify. After your submission, one of our customer support agent will personally assist you within 1 to 3 business days.  

Before diving deep into the nuts and bolts of starting an exciting online business, please take a look at the outline below to assist with your navigation and learning.  

---
- [Information Portal](/customization/README.md)  
---
- [Video Tutorials Portal](https://www.youtube.com/channel/UCG-8TGzkP7XhKKxTj261o1w)  
--- 
- [Our Themes](https://themeforest.net/user/arenacommerce/portfolio)  
---
- [Frequently Asked Questions and How Tos](/faq/README.md)  
---
- [Support Guidelines & Policies](/quickstart/support.md#_3-support-policy)
---  


## Beginner Essentials
In this section, we will list the most essentials a new Shopify store owner needs to study to bring their store public smoothly. While you can always hire professional helps from us and the Shopify team, it is best to take matters to your own hands or at least know what should be done to ensure productivity and time-saving.  
Some of the posts we included are warmly provided by Shopify and you may have come across them. Some are the results from thousands of feedbacks from our treasured customers that we think will be of great value to you.  
Enjoy!  

---
- [Beginner's Overview of the myShopify Admin Homepage](https://help.shopify.com/en/manual/intro-to-shopify/shopify-admin/shopify-admin-overview)  
---
- [Essential Setups For A New Store](https://help.shopify.com/en/manual/intro-to-shopify/initial-setup)  
---
- [Installation Requirements](/xstore/themeinstall.md#step-1-installing-required-app-and-extension)  
---
- [Installing Our Theme](/installation/dashboard.md)  
---
- [Begin Customizing Your Theme](/customization/startcustom.md)  
---
- [Get The Best Out Of Theme Customization To Boost Your Business]()  
---
- [Store Aesthetic - Design Tricks From Top Brands]()  
---
- [Saving The Most For Your Business With Our Partners]()  
---
- [Go Multilingual With Weglot](/xstore/apps.html#weglot)
---  


## Installing Our Theme  
---
- [Preparing your Shopify Store](/installation/README.md)
---
- [Locating Your Theme Purchase Code](/installation/purchase-code.md)  
---
- [Installing Required App and Extension](/installation/dashboard.html#step-1-installing-required-app-and-extension)  
---
- [Adding Your License Code to Activate The Theme Installation Process](/installation/dashboard.html#step-2-adding-your-license-code)  
---
- [Install the theme](/installation/dashboard.md#step-4-install-the-theme)  
---


## Update & Maintenance
The update process will be similar to other themes updates, which you can take a look [here](/electro5/update.md#electro-5-update-to-electro-6).  
We will include update instructions for further versions of XStore upon release.  
For further information, please go [here](/xstore/update.md).

## Backup Your Store
You may think that Shopify backs up your store, but in reality, **Shopify only backs up its platform**. You are only one mistake, bug, or worst - attack away from unwanted downtime, lost revenues, customers outrage and an expensive rebuild. Our friends at [Rewind](https://rewind.io/backup/shopify/) offer ingenious solutions to this problem with their brilliant backup plan.
From Basic plan of only $3/month to Enteprise plan for your million-dollar business, they have got you covered!  

You may also want to **duplicate your theme first** before customizing from comparison purposes to data preservation. For that, Shopify had already has an intuitive, easy-to-do article [here](https://help.shopify.com/en/manual/using-themes/managing-themes/duplicating-themes).  