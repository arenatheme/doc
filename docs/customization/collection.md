---
metaTitle: Customizing Collections - ArenaCommerce Help Center
sidebarDepth: 2
---
# Customizing Collections  

Since you are using Shopify platform, most of the configurations regarding Products, Collections, Metafields, etc... how-tos are already presented in the [Shopify Help Center](https://help.shopify.com/).  

As for the Collections tutorial, our friends at Shopify have made a very intuitive, in-depth tutorial [here](https://help.shopify.com/en/manual/products/collections).  

For the Arena User Guide, we will go into details about how our themes customization tools help optimize Shopify's basic functions even further so you can better adapt to customers needs and increase sales conversion. Please refer to each theme's User Guide to find details about how to customize each function according to that specific theme. You can find each theme's Collections setting guide below:  

---
- Electro 5.0 - 6.0: [Collections Guide](/electro5/collections.md)
---  
- XStore 1.0+: [Collections Guide](/xstore/collection.md)
---  
- Handy 4.0+: [Collections Guide](/handy4/collections.md)  
---
- Zeexo 1.0+: [Collections Guide](/zeexo/collection.md)
---  





