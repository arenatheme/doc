---
metaTitle: Adding Media Files - ArenaCommerce Help Center
sidebarDepth: 2
---
# Adding Media Files  
Adding media files to a product is essential for every store since it is the first thing customer used to decide whether they would make the purchase. Shopify allows you to add most media files including images and videos. Let's dive in.   
 
## **Images**  
Shopify is compatible with most frequently used image files from .jpeg to .png, .gif and even 3D .glb files.These can be used to display your products on the product cards or in the description as well.  
- **For file size**: we recommend images that are 1024 x 1025px with a file size smaller than 20 MB; maximum size could be 4472 x 4472 px.  
- **File type**: the best file type for most product images is .jpeg since they can be optimized for loading speed, although you may want to add some .gif to your product card or in the description to further illustrate your product's image in the customer's eye.  
:::warning Auto structurizing function 
While all of our themes have a function that will automatically cropped and resize your uploads to ensure page layout integrity, we recommend uploading product images of the same dimentions and ratio to get the best out of this feature. You can find options to configure the way product card's image appear on both Product Grid and Product List.  
It can also be turnt off for your convenience but we cannot guarantee the integrity of the page's layout should you decide to choose this option.
To access this funtion and all other functions relating to product card, simply go to:  
**Theme Editor > Theme settings > Product Card**
:::  
![image-ratio-settings](/image-ratio-settings.png)  

## **3D models**  
3D models are virtual representations of your chosen product in three dimensions. They let you view the object from any angle. With a clear background, your customers can put your product into their environment to see how it would fit into their lives with a better sense of the size, scale, and details. You need to make sure that your 3D model files meet the following requirements before you add them to your online store:  
- **File size**: should be 6MB or under; maximum size of 15MB accepted.  
- **File type**: .glb  
You can hire a Shopify Expert to help you create quality 3D models of your products.  

## **Videos**  
You can add videos for your product in two ways: uploading a video file, or embedding a YouTube video link. If you're uploading video files, then make sure that they meet the following requirements:  
- **Video length**: Up to 60 seconds  
- **Video size**: Up to 1 GB  
- **Video resolution**: Up to 4K (4096 x 2160 px)  
- **Video file type**:.mp4 or .mov  
:::note *If you're embedding YouTube videos, you do not need to meet those requirements.*  
 
## Adding Media to Product Page  
The first media item for each product is known as the featured, or main, media item. This item would be shown on collection pages, the cart page, the checkout page, and your home page as an element of the Product Card.  
**Step 1**: From your myShopify Admin Homepage, click **`Products`**.  
**Step 2**: Select a product or add a new product.  
**Step 3**: Add an image, 3D model, or video in one of the following ways:
- Click **`Add media`** and then select the file that you want to upload.  
- Drag and drop the file that you want to upload onto the Media section.  
- Click **`Add media from URL`** to add an image or video from a URL.  
:::theo While there are plenty of ways to use third-party services to import images...
It is better to use the traditional image uploading ways because using third-party services have great down-sides: it affects your page loading speed greatly, the sheer quantity of the images having to load at one time can break the third-party services API creating errors, leading to color swatch and some relating functionalities losing its funtions due to the lack of API, etc...
:::  
![add-media](/add-media.png)  

## Adding Media to Product Description  
![add-media](/add-media2.png)
Adding media to product description is almost the same as adding them into the product, except you cannot drag and drop the image into the product description text box. Simply put the text pointer into the place where you want to add the image and click on the image button.  

The only difference is adding a video.  
After clicking the video button, a textbox will appear requiring you to paste in the embedding code as seen below.
![embed-youtube](/embed-youtube.png)  
To find this code 'snippet', you need to go to the video you want to embed, click **`Share`** > Choose **`Embed`** > Copy the code snippet in the **Embed Video** text box.  
|   |   |   |
| - | - | - |
| ![share-youtube](/share-youtube.png) | ![share-youtube](/share-youtube2.png) | ![share-youtube](/share-youtube3.png) |  
:::warning 3D files cannot be uploaded into product description
Another alternative is to upload .gif files or embed videos so your customer can have a better visual experience.
:::

