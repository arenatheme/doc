---
metaTitle: Currency & Multi Currency - ArenaCommerce Help Center
sidebarDepth: 2
---
# Currency & Multi Currency   
When you take your business international, the first step to engage customers from different countries is to give them an impression of your items cost in their currency. Please know only stores using Shopify Payments can use this feature and only selected countries can be supported. To see if your country is on the list and if your business is eligible, click [here](https://help.shopify.com/en/manual/payments/shopify-payments/shopify-payments-requirements#supported-countries-and-regions). 

Once you have verified with Shopify of your eligibility, you can enable this feature in the myShopify Admin Homepage.
- Choose the tab `Theme settings` > `Multi-Currency`.
![multi-currencies](/multi-currencies1.png)  

From here, you have 2 multi-currency options to choose from:  
- [x] `Multiple Currencies By Shopify` - this option will enable customers who prefer to pay through Shopify-affiliated payment methods to see the item price in their currency at the exact exchange rate these methods offer.  
- [x] `Multiple Currencies By ArenaCommerce` - this option will enable customers to see the items price in their currency. To enable this, you need to set up the currencies on the dropdown menu below the `Auto Currency Switcher` option.  
For this tutorial, I choose the Default Currency as the same (EUR) as the ones I set up in the [Preparing Your Shopify Store](/installation/preparing-your-shopify-store.md) tutorial at the beginning of the store set up process.  
    :::theo If you decided to set up a different currency
    Than the one you set in the previously mentioned tutorial, you can use the `Multiple Currencies By ArenaCommerce` option, **your store's default currency will then switch to the one you set in this option**.
    :::  

    You can also set the `Money Format` option to display the format you want as seen below:
    | <center><button class="btn">1</button></center> | <center><button class="btn"><i class="far fa-arrow-alt-circle-right"></i>2</button></center> | <center><button class="btn"><i class="far fa-arrow-alt-circle-right"></i>3</button></center> |
    | --------------------------- | ---------------------------- | --------------------------- |
    | ![](/multi-currencies2.png) | ![](/multi-currencies3a.png) | ![](/multi-currencies3.png) |
      

- [x] `Disable` - this option will disable currency conversion completely and only show products in the currency you set up in General Settings when you first install the theme.  

You can also enable auto currency switcher on your site to help customers browse your site undisturbed. Simply tick on the [x] `Auto Currency Switcher On Customer Location` then go to 'Currencies to support' field to add your currencies.  

The option to display the currency flag at the upper right corner of your site is also available by choosing one of these choices in `Currency flags` at the end of this function tab:  
    - Hidden  
    - Rectangle  
    - Square  
    - Circle  