---
metaTitle: Create and Customize a Shopify Page - ArenaCommerce Help Center
sidebarDepth: 3
---
# Create and Customize Page

## How to Create a Shopify Page

![Add Shopify Page](/page_add_op.png)
1. From your **`myShopify Admin Homepage`**, go to **`Online Store` > `Pages`**.  
2. Click **`Add page`**. You will be taken to a new webpage editor.  
3. In the webpage editor, enter a **`Title`** and **`Content`** in the text boxes provided.  
    ::: theo
   Be descriptive and clear when choosing your webpage title. The webpage title is displayed in the tab or title bar of browsers. It is also used as the title in search engine results. [Learn more about website SEO](https://help.shopify.com/en/manual/promoting-marketing/seo).
    :::  
4. In the **`Visibility`** field, you can select whether you want your webpage to be published or not. By default, your new webpage will be visible when you click **`Save`**. Select the **`Hidden`** option if you want your new webpage to be hidden from your online store, or click **`Set a specific publish date`** to [control when your webpage is published](https://help.shopify.com/en/manual/productivity-tools/future-publishing).  
5. In the **`Template`** field, you can select page template \(page type\) that you would like to apply.
:::warning There could be more than 1 template
Depending on the theme, don't forget to **`Publish theme`** before creating any page.  

:::
::: right Read more: Official Shopify Guide
[Alternate template Shopify Document](https://help.shopify.com/en/themes/customization/store/create-alternate-templates)
:::
6. Click **`Save`**.

## Customize a Shopify Page with Section
Before diving into customizing, we will get you familiar with out page system. There are 22 pages all our themes have. They are:
|   |   |
| :--- | :--- |
|- Home page.<br>- Product page.<br>- Collection page.<br>- Collection list.<br>- Blogs page.<br>- Blog post page.<br>- Cart page.<br>- Checkout page.<br>- Error 404 page.<br>- 'About Us'/ Testimonials Page.<br>- FAQ page. | - Wishlist page.<br>- Compare page.<br>- Contact page.<br>- Gallery page.<br>- Information/ Terms and Conditions page.<br>- 'Our team'/ Team member page.<br>- Shipping Policies page.<br>- Search Results page.<br>- Shipping & Return page.<br>- Size chart page.<br>- Order Tracking page. |  
All of them are comprised of the same elements and a few unique elements that help them execute their functions.  
![](/page_customize.png)

To start customizing, go to your myShopify Admin Homepage:
1. In the **`Online Store`** tab, click on **`Theme`**.
2. Find the theme that you want to edit and click **`Customize`**.
3. From the top bar drop-down menu, select the page that you want to configure.
4. Start configuring from the tab **`Section`**.
5. Add Page Content (if it available with pre-define pages template the theme support).
6. Click **`Save`**.

There are some customizations that can be done for the whole theme including every page in it. These customization options can be found in the **`Theme settings`** tab instead, including:  
- General settings: Layout, Background/ Background Body Page.  
- Typography.  
- Fomo popups.  

For the customization of each section that all our page templates share, please see below.

### Announcement Bar  

### Header  

### Text Slider  

### Footer Widgets  

### Footer  

### Mobile Bar  

### Newsletter Popup  

### Custom CSS  

For customizing the unique elements of each page type, please check the **Pages** section in each theme's User Guide to learn specific tweaks and modification of individual page type belongs to that theme.

## How to Create a Store Locator Page
1. Repeat step 1 to 4 of the **'How to Create a Shopify Page'** above.  
2. In the **`Template`** field, select ***'page.store.locator'*** template. Here I named the page 'Our Stores' for easy recognition.
3. Click **`Save`**.  
4. Click **`View page`**.  
![View page](/View_page.png)  
5. Go to Theme Editor, choose **'Our Stores'** from the page dropdown menu.  
![choose page](/Our_stores.png)  
6. Go to **`Theme settings`** > **`App Integrations`** > **`GOOGLE MAP`** and enter your API key. To find out, click [How to Create a Google Map API Key](https://arenathemes.freshdesk.com/support/solutions/articles/6000209417-creating-a-google-maps-api-key). Click **`Save`**.  
![API-key](/API-key.png)  
7. Go back to **`Section`**, click on **`Store Locator Page`** tab > click **`MAP CONFIG`**.  
- [x] Geolocation service: Select this to use Google location service to help customers find the stores near by.  
- When you disable 'Geolocation services'. Type by Latitude and Longitude in the format: `Latitude, Longtitude`.
::: theo
[Get your Store Latitude and Longitude here](https://www.latlong.net/).
:::  
### Add Store Location  
1. Click **`Add stores`** at the bottom
2. Fill in **'Store name'**.
3. In the **'Store info'** section, type in your store location address.  
4. Fill in **'Operating hours'**, instead of clicking Enter on your keyboard to move down a row, type in <br> with no space between it and the letters instead.  
5. Fill in **'Store location'** in the format: `Latitude, Longtitude`.
6. To add more locations, simply click **`Add stores`** at the bottom and repeat.  
7. To change map style, choose the style [here](https://snazzymaps.com/), click **'Expand code'**, then copy all the codes and copy into **'Map style'** text box.  
![snazzy](/snazzy.png)  
8. You can also set the Location Pin's color and size below the **'Map style'** text box.  
9. Click **`Save`**.

The final page would look like this:
![location-final](/location-final.png)  