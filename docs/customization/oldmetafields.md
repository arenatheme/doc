# Using Custom Fields in Your Shopify Theme

You can use metafields to add custom fields to objects such as products, customers, and orders. Metafields are useful for storing specialized information, such as part numbers, customer titles, or blog post summaries. They can be used by apps and channels to track data for internal use.

Advanced Custom Field (ACF) a powerful application for your Shopify store that utilizes Shopify's Metafields. It enables admins to add structured metafield fields (custom fields) to things in Shopify (objects), such as products, collections, orders, customers, and pages. These extra content fields are more commonly referred to as Custom Fields and can allow you to build websites faster and educate your clients quicker.

More detail [https://help.shopify.com/en/manual/products/metafields](https://help.shopify.com/en/manual/products/metafields)

![](https://help.arenacommerce.com/acf/acf.jpg)

---

## 1\. Installing Advanced Custom Field (ACF)

![](https://help.arenacommerce.com/acf/acf_install.gif)

* _Step 1_: Go to [Advanced Custom Field](https://apps.shopify.com/advanced-custom-field) in the Shopify App Store after you logged in to your myShopify account.
* _Step 2_: Click the `Add App` button.
* _Step 3_: Log into the App Store.
* _Step 4_: Confirm App installation by click `Install App`.
* _Step 5_: Use the app! Your newly installed app will live in the App section of your Shopify Admin.

## 2\. Using ACF to extend Your Store Content

### Quick Start

**ACF** supports import the **default metafields that we use in most themes.**

* _Step 1_: Go to Apps > Advanced Custom Field > Fields > Products
* _Step 2_: Click Import Button
* _Step 3_: Click Save Button  
Now you can go to Editor tab to edit your product custom fields content.  
![](https://help.arenacommerce.com/acf/acf_import.gif)

:::tip Note 
- All of the following metafields are for resource Products.
- To know which metafields in the list below can be used in your ArenaCommerce theme(s), please refer to corresponding theme's documentation or [contact us](https://arenathemes.freshdesk.com/support/tickets/new).
:::  

**You may also following instruction below to add custom field manually**

### Product Short Description / Exception Description

* _Step 1_: At Field tab, create a HTML Field Product Description.![](https://help.arenacommerce.com/acf/acf_description.gif)

How to [add new field](https://help.arenacommerce.com/acf/fields-tab.html#add-new-field).

* _Step 2_: Click Save button to save your changes; then go to Editor tab's resource "Products" and pick a product to start with.

Use search bar to quickly [find specific products](https://help.arenacommerce.com/acf/search.html#products).

    - namespace: c_f - key: description_excerpt - label: Short Description 

* _Step 3_: Click Save button to save your changes; then go to Editor tab's resource "_Products_" and pick a product to start with.![](https://help.arenacommerce.com/acf/acf_description_editor.gif)
* _Step 4_: On next page, pick a product item for each field editor via resource picker. Add your product short description.![](https://help.arenacommerce.com/acf/bundle-product.png)
* _Step 5:_ Click Save button to save your changes.

_Use left - right arrow buttons to navigate to previous - next items._

### Countdount Timer

* _Step 1_: At Field tab, create a [date field](https://help.arenacommerce.com/acf/types-of-fields.html#_7-date)

    - namespace: c_f - key: countdown_timer - label: Date format is mm/dd/yyyy 

It is required to set date format as _mm/dd/yyyy_ in order for countdown to work.

* _Step 2_: Click Save button to save your changes; then go to Editor tab's resource "_Products_" and pick a product to start with.
* _Step 3_: On next page, pick a date
* _Step 4_: Click Save button to save your changes.  
Countdount Timer is all set up.

### Bundle Product

* _Step 1_: At Field tab, [create a group](https://help.arenacommerce.com/acf/fields-tab.html#add-new-group) called Bundle Product.
* _Step 2_: Create 2 [product reference fields](https://help.arenacommerce.com/acf/types-of-fields.html#_13-product) in that group

    - namespace: c_f - key: bundle_1 - label: Add your product handle #1! 

    - namespace: c_f - key: bundle_2 - label: Add your product handle #2! 

* _Step 3_: Click Save button to save your changes; then go to Editor tab's resource "_Products_" and pick a product to start with.
* _Step 4_: On next page, pick a product item for each field editor via resource picker.![](https://help.arenacommerce.com/acf/bundle-product.png)

Use left - right arrow buttons to navigate to previous - next items.

* _Step 5_: Click Save button to save your changes.

And... That's it! Bundle Product is all set up. You can refer to our example work at [https://electro.arenacommerce.com/products/gold-diamond-chain](https://electro.arenacommerce.com/products/gold-diamond-chain)

### Exit Intent Popup Product

* _Step 1_: At Field tab, [create a group](https://help.arenacommerce.com/acf/fields-tab.html#add-new-group) called Exit Intent Popup Product.
* _Step 2_: Create 2 [text fields](https://help.arenacommerce.com/acf/types-of-fields.html#_1-text) in that group

    - namespace: c_f - key: discount_code - label: Add your Coupon code for the product 

    - namespace: c_f - key: discount_percent - label: Display percent of discount 

* _Step 3_: Click Save button to save your changes; then go to Editor tab's resource "_Products_" and pick a product to start with.
* _Step 4_: On next page, add your coupon code and display percent of discount
* _Step 5_: Click Save button to save your changes.

Exit Intent Popup Product is all set up. You can refer to our example work at [https://electro.arenacommerce.com/products/black-fashion-zapda-shoes](https://electro.arenacommerce.com/products/black-fashion-zapda-shoes)

## Bulk editing product metafield

As we've seen, metafields have three components: `a namespace, a key, and a value`. Once you know the namespace and key for your metafield, you can display it in [the bulk editor](https://help.shopify.com/en/manual/productivity-tools/bulk-editing-products) by making small changes to the URL of the bulk editing page in the Shopify Admin.

Eg. Collection page: [https://shopify.com/admin/bulk?resource\_name=Collection&edit=metafields.c\_f.subcategory:string](https://shopify.com/admin/bulk?resource_name=Collection&&edit=metafields.c_f.subcategory:string)

In this URL, `c\_f` is the `metafield’s namespace` and `subcategory` is its key. You can replace these with values to match your use case. The `string` part of the URL tells Shopify about the type of data that we want to store in the metafield. The bulk editor is able to display five types of metafields -- `string`, `money`, `boolean`, `select` and `number`