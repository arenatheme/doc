# Electro Cart Section

::: theo
Refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Electro Theme` > `Customize` >  `Sections` > `Cart`
:::

![Electro Collection Cart Page](/electro5/cart_section.png)

## Cart Configuration

![Electro Collection Cart Page](/electro5/cart_section_1.png)
