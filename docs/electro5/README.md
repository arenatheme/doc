---
metaTitle: Electro 5.0 Shopify Theme User Guide - ArenaCommerce
sidebarDepth: 2
---
# Electro Theme Features

[[toc]]

<!-- - Shopify Installation App: Arena Dashboard -->
![Electro 5 Shopify Theme Preview](/electro_preview.png)  
  
</br>
</br>  

- Layout Boxed & Wide - Boxed mode: 1280px & 1440px
- RTL Build-in
- Lazy loading Image
- Arena Font Icons Integration
- Full Page Sections: Homepage, Collection, Blog, Cart...
- Advantage Typography with Shopify Fonts.
- Shopify Review Integration
- [Multiple Header Designs](/electro5/header.md)
    - Flexible Header Positions
    - Sticky Header Setup & Options
    - Unlimited Mega Menu
    - Mobile Menu
- Build-in Wishlist & Compare by Free Arena Wishlist & Compare Shopify app
- Build-in Quick View
- Catalog Mode
- Unlimited Layout Collections
- Prefine Page Templates with specific block layout
    - [Store Location](/electro5/page-templates.md#store-location-page)
    - About
    - Contact
- [Prefine Product Templates](/electro5/product-templates.html#product-templates)
    - Product Simple
    - Product Extended
    - Product Bundle
    - Product Redirect
- Build-in [Live Ajax Search](/electro5/general.html#search-box)
- Build-in Multi Currency Support
- EU Cookies Policy Notify - GDPR Compatibility
- Newsletter Popup - support Mailpchimp
- Always up to Date
