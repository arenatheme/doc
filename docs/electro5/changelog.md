
# :sparkles: Electro Changelog
## Update 7.2 - Jan 2020
- New: Support Multilingual with ATranslate app
- New: Auto change layout RTL / LTR according to language translate
- Improved: Fix bugs
## Update 7.1
- Fix some bugs
## Update 7.0 - Oct 7th, 2020  
- New cutting edge layouts with flyout mega menu!  
| Home V8 | Home V9 |
| --- | --- |
| ![](/home-8.png) | ![](/home-9.png) |  
![fly-out-megamenu-home9](/fly-out-megamenu-home9.gif)
- Now with auto RTL layout switch with language change on [Weglot](https://apps.shopify.com/weglot)!  
- Improved: Fixed Electro 6.0 bugs.  

## Update 6.0 - May 7th, 2020  
- New: Shopify Multi-currency support Shopify Payment Checkout.  
- New: Support Loox - Reviews apps.  
- New: Support Growave - Reviews, Loyalty ++ App with 30 days trial extended offer.  
- New: Unique sub-collection & collection sidebar by metafield. [How it work?](/electro5/collections.html#_4-add-collection-pages-content)
![](/electro6/sub-collection-v6.gif)  
- New: Update Jquery 3.5  
- New: Free Wishlist & Compare app compatible with Chrome 80+  
- Improved: Instant layout change  with Theme Options.  
- Improved: Website speed.  
- Improved: RTL layout, support auto-switch base on location.  
- Fixed: Bugs related to currency, languages,...  
- Remove integration code Judge.me product reviews code.  

## Update 5.0 - July 4th, 2019  
- Improved performance for large inventory stores.  
- Layout: RTL.  
- Integration apps: Judgeme, Searchise, Weglot js.  
- Mega Navigation improved with accept add multi Vertical Tab.  

## Update 4.1 – Nov 11th, 2018  
- New Unlimited Mega Menu with new type: Vertical tab.  
- Improved Mobile UX for Mega Menu  

## Update 4.0 – Sept 18th, 2018  
- Major update: Update full sections pages control panel  
- Replace CartJS by Line item API to adapt other Shopify apps  
- Add more function as description  
- Upgrade Bootstrap 3 to Bootstrap 4  

## Update 3.0
 - Add 2 more homepage styles  
- Enhance Design Home 1,2,3  
- Fix some bugs  

## Update 2.0  
- Support Shopify Section
- Fix bugs: Wishlist, Compare  

## Version 1.2 - Sept 28th, 2016  
Feature update:  
- Product compare  
- Fix bugs  

## Version 1.1 - June 28th, 2016  
Feature update:  
- 4 styles Grid/List : Grid, Grid Extended, List, List Small  
- Update json language file  
- Fix bug config color settings  
- Cart page: UpSell Products  
- Product page: Multi tab  
- Free shipping bar app.  

## Version 1.0