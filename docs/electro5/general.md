---
metaTitle: Electro Shopify Theme General Settings
sidebarDepth: 2
---

# Electro Theme Settings


This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Electro Theme ` > `Customize` > `Theme Settings`

![](/go_to_theme_setting.png)

## 1. Apps Intergration

Step 1: [<button class="btn">Installing Shopify Apps</button>](./installation/app.md)  

Step 2: Configure Apps & Theme Setting

```
Online Stores > Themes > Electro Theme > Customize > Theme Settings > Apps Intergration
```

### WISHLIST & COMPARE


:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Installing Arena Wishlist & Compare App" id="wishlist-compare-settings"
Read more: [Install Arena Wishlist Compare app](/installation/apps.md)
:::
It's our free Shopify app to support Customers can create a Wishlist by logging into their store account and simply start saving their favorites to it for later viewing.

::: tab "Arena Wishlist Compare Settings" id="wishlist-setting"
Wishlist & Compare is not offered by Shopify as a function so we have built a Free Shopify Application to assist customers.
- **Wishlist** is build by using Shopify App Proxy, it's not required to setup template page on your Shopify store. Wishlist function will launch at: https://your-shopify-url/apps/wishlist
- Compare required create and assign a separate page for the Compare page display
:::
::: tab "Theme Settings" id="wishlist-theme-settings"
- [x] Enable Wishlist
- [x] Enable Compare
:::
::::

::: right
Read more: [Install Arena Wishlist Compare app](/installation/apps.md)
:::
### PRODUCT REVIEW

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: true }"
::: tab "Theme Settings" id="theme-settings"
Product review by:

- [ ] None: Disable / Hide product review
- [ ] Product Review: Using Shopify Product Review app
- [ ] Judge.me: Using Judge.me app
:::
::: tab "Judge.me Settings" id="judge-setting"
#### Judge.me
It's require to select install Judge.me Review Widget Installation `Manually` because we already integration into the theme code.
![](/judgeme_manual.png)
:::
::::

### SEARCH BOX

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: true }"
::: tab "Theme Settings" id="theme-settings"
- [ ] Show Search Box: Show search box in the header
- - Auto Suggestion Search by
    - [ ] None: Disable auto suggestion in search box
    - [ ] Default: Using auto suggestion by code build-in theme
    - [ ] Smart Search app: Using Smart Search & Instant Search by Searchise
:::
::: tab "Smart Search & Instant Search" id="smart-setting"
#### Smart Search & Instant Search <Badge text="Discount 20%"/>
Electro support `Instant search widget`. You may enable it by select Smart Search app in Theme Settings & Enable this function in app
- Go to Smart Search & Instant Search dashboard > Instant search widget
- Select `Enable instant search widget` &  `Optimize for phones and tablets` > click `Apply changes`
![](/search_instant.png)
:::
::::
### LANGUAGE TRANSLATOR - WEGLOT APP
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: true }"
::: tab "Theme Settings" id="theme-settings"
- [ ] Enable language translator: Using Weglot.js to translate the website into other languages.
- [ ] Box width: Language modal popup width size
- [ ] API Key: Your Weglot API key.
- [ ] Original language: default language
- [ ] Destination languages: Choose languages you want to translate into. Supported languages can be found [here](https://weglot.com/documentation/available-languages).
- [ ] Flag Type: Flag sharp in Header
:::
::: tab "Weglot Register & Settings" id="weglot-setting"
#### Weglot Free?
After signing up, you have a  10-days free trial period to test Weglot. Once the 10 days free trial ends, you have 2 possibilities:
- Your website has less than 2000 words, and you want to translate it only in one language: You can use the Free plan. The Free plan is automatically chosen at the end of the trial period, you don't need to choose it manually.
- Your website has more than 2000 words: You will need to upgrade your plan to continue using Weglot.
You can find your number of translated words in your account, on your dashboard.
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/5d0a37de2c7d3a1cad5b8f13/file-VIzht1Oy3D.png)
Detailed pricing:
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/5d0a389b2c7d3a1cad5b8f21/file-uJgnrENCUU.png)
#### Register Weglot with 20% Discount <Badge text="Discount 20%"/>

- From your Shopify admin, go to `Apps > Arena Dashboard > Perk`
![](https://cdn2.shopify.com/s/files/1/0208/3880/9664/files/dashboard_large.png?v=1563251258)
- Hover to Weglot & Click **Claim Perk** to get promotion code (discount 20%) & go to register website Weglot.
Please save Promo code & you can redeem it later when you want to upgrade the service.
- Enter `your email`, `password`,  check `I agree to Weglot's Terms of Service` and click **Start Free Trial**.
- Verify your email address and go to Weglot Dashboard.
- From your Weglot Dashboard, you can access the projects tab at the very top left. Once you click on it, you'll see a drop down with a green button "Create project" to add a new project (see below).From this tab, you can also select the project you want to work on.
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/59e620d22c7d3a752de12a7f/file-wWBkWK7Dom.png)
- Enter your Project name, select website technology is Shopify.
- Click Next to create Project
- You can find the different project settings detailed below on your Weglot account, under "project settings".
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/5d0a28022c7d3a1cad5b8e1a/file-B5tX8sHUdP.png)
    - **Name and website URL**
        Give a name to your project to easily navigate between your different projects. 
        ::: theo
        ![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/59e62b6d2c7d3a752de12afc/file-KURaNthtH6.png)
        In order to get the Visual Editor working properly, please make sure to add the correct website URL in the settings.
        :::
    - **API key**: A separate unique API is created for each project.
        ![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/59e62acc2c7d3a752de12af4/file-FzX2fu2bmz.png)


:::
::::

## 2. Multi Currency

::: theo Preparing Multi Currency Switcher
[<button class="btn">Preparing your store for Multi Currency</button>](/installation/#preparing-your-shopify-store)
:::
```
Online Stores > Themes > Electro Theme > Customize > Theme Settings > Multi Currency
```
Group Parameter | Description
-|:-
Enable Multi-currency | Enable multi currency switcher
Auto Currency Switcher On Customer Location | Use **Geo IP Location** to detect your customer location and auto select the store currency||
Flag type | - Retangle/ Square/ Circle: Show country-currency flag & select sharp
^^|
^^| - None: Flag Hidden
Money Format | Select money format
Currencies You Wish To Support | - All: Show all currency Shopify support
^^|
^^| - Select by manually: select by list below
Currencies You Wish To Support Manually | Input Currencies Code, separate your currency codes with a space

## 3. General Settings

```
Online Stores > Themes > Electro Theme > Customize > Theme Settings > General Settings
```  
|| Parameter | Description
:-:|:-|:-
Enable lazy loading images || Lazy loading is technique that defers loading of non-critical resources at page load time. <br>Instead, these non-critical resources are loaded at the moment of need. <br>Where images are concerned, "non-critical" is often synonymous with "off-screen".
**LAYOUT**|Layout Mode| Configure Wide / Boxed (Container) Layout
^^|![](/wide_box.png)||
^^|Fix Boxed 1280px| Configure Boxed Layout as 1280px instead of 1440px
^^|![](/box_1280.png)||
^^|Switch to RTL layout| Change your website layout to Right to Left
^^|Button Back To Top | Show button Back to Top
^^|^^|^^
BACKGROUND BODY| You may select background image or color for body content||
BREADCRUMB| Configure breadcrumb detail: background color/images, height||
SHOP| Enable Catalog Mode | Disable Add to cart button
^^|![](/catalog_mode.png)||
^^|Display messages to remind free shipping offer| Display shipping threshold message
^^|Free shipping total cart require| Enter the number of total cart to free shipping condition</br>Currency: base on store setting
^^|![](/shipping_threshold.png)||
STATIC SECTIONS| Homepage Static Sections | Setting to enable/disable sections for homepage

### Announcement Bar - TopBar Sections
- [x] Announcement Bar: Enable Homepage Announcement Bar Static Section
![](/top_bar.png)
### Mobile Menu Icon
- [x] Mobile Department: Enable Mobile Menu with icon Section
![](/menu_mobile_icon_section.png)

### Mobile Bottom Bar

![](/inside_mobile.png)


## 4. Typography

Parameter | Description
--|:-
HEADER FONT | Configure Heading font family
Heading 1-6 Size | Configure Font Size Heading H1-H6
BODY FONT | Configure Body font family
Body size | Configure body font size
Top Bar font size | Configure Top Bar (Announcement Bar) font size
Navigation size | Configure Navigation font size
^^ | ![](/typo_top.png)
^^ |
Navigation dropdown size | Configure Navigation dropdown font size
Product title size | Configure font size Product name in Product card
Product item title size | Configure font size Product name in product page
Product item price size | Configure font size Product price in product page
^^ | ![](/typo_item.png)
^^ |
## 4. Icon
```
Online Stores > Themes > Electro Theme > Customize > Theme Settings > Icon
```  
The icons setting use in whole store pages. You can checkout all font class here: 

Parameter | Description||
:-|:-|:-|
Favicon | Favicon of website.|**Icon type**: select `font icon` or `image`</br></br>**Font icon name**: insert icon font class name start with icon</br></br>[<button>Find Arena Font icon class name</button>](https://font.arenacommerce.com/electro5)</br></br>[<button>How to add more icons</button>](https://font.arenacommerce.com/electro5)</br></br>**Image**: upload image file</br>
Loading icon | Loading icon|^^
Phone number icon | Phone icon in header & others|^^
Email icon | email icon in header & others|^^
Track order icon| icon track in header (delivery)|^^
Location icon|icon location in header & others|^^
Account icon| icon account|^^
Wishlist icon | icon wishlist|^^
Compare icon | icon compare|^^
Cart icon | icon for cart in header & product cards|^^
Social icon | icon for social sharing button|^^

## 5. Colors

Parameter|| Description
:-|:-|:-
Header|Background color| Background color for header
^^| Text color| Color for header text
^^| Background for the number on icons | Phone icon in header & others
^^| Email icon | Email icon in header & others
^^|![Background](/header_color.png)||
Body color | Main Color | Primary color use for main site
^^|Background| Color body background
^^|Text color| Body default text color
^^|Miscellaneous Color|Text for misc detail
^^|General Text Color|Text color in dark site
^^|General Light Text Color|Text color in light site
^^|Title Text Color|General title color
^^|Link Color|General Text Links color
^^|Border Color| General boder color
^^|Ratting Empty Color|Star rating empty color
Navigation color| Link color | Nav Text links color
^^|Link Hover Color| Nav Text link when hover color
^^|Background Color In Dropdown| Background color for dropdown nav
^^|Link Item Color In Dropdown| Text links in dropdown nav color
^^|Link Item Hover Color In Dropdown| Text links when hover in dropdown nav color
^^|Background Navigation Hover Color| Background when hover color - only for header style 03
Navigation Label Color|Text sale label color|Text color in `Sale label`
^^|Sale label background| Background color `Sale label`
^^|Text new label color|Text color in `New Label``
^^|New label background| Background color `New label`
^^|Hot label color|Text color in `Hot Label``
^^|Hot label background|Background color `Hot label`
^^|![Background](/nav_label.png)||
Footer Color|Footer Content Background|Background color
^^|Footer Text Color| Footer text color
^^|Footer Link Color| Footer text links color
^^|Footer Link Hover Color| Footer text links hover color
^^|Footer Copyright Background| Footer bottom copyright container background
^^|Footer Copyright Color| Copyright text color
Button style 01| Custom button style 01: background, text, border, hover...||
Button style 02| Custom button style 02: background, text, border, hover...||

## 6. Fomo Popups
::: tip
Fomo Popups display real-time customer activity notifications on your store. You can use it as a Social Proof Markteting Tool to Boost Conversions.
:::

### Sale Notification Popups

Popups to show the number of Sale on your Online Store to encourage customer making purchase.

#### Sale Notification Popup
![](/fomo_demo.png)

**Sale popup template:**
::: theo
Some one in `{Random City}` `{Random Text}`.  
`{Random Product in Collection}`.  
`{Random Number}` `{Random Text}`  
:::


Parameter|| Description
:-:|:-|:-
Popup Position||Popup position to display on Desktop when lauch.<br> Position: Bottom Left/Bottom Right/Top Right/Top Left.
Visibility|Show On Home Page | Show Sales Notification on Home Page
^^|Show On Product Page | Show Sales Notification on Product Page
^^|Show On Collection Page | Show Sales Notification on Collection Page
^^|Hide on Mobile Device | Hide Sales Notification on Mobile Device
{Random City}|Random City base on | **GEOIP**: Auto Dectect Visitor Country and get list all cities to use <br> **Manual Cities List**: Input optional Cities in next field <br> **Combine both list**: Mixup cities list in visitor country & manual cities list.
^^|Manual Cities List | Your manual cities list. Separate city by **;**
{Random Text}|Random Text | Random text. eg: just bought;view;search
{Random Product in Collection}|Random Product in Collection | Select product in Collection to show
{Random Number}|Random Number | Select random number to show. Separate number by **;**
{Random Text}|Random Text | Random text. eg: minutes ago;hours ago;days ago
Background color||Background color 


### Visitor Count Popups

Popup to show Random Visitor detail to Product page.
![](/number_visitor.png)
**Visitor Count Template**:

```
{Random Number} of visitor {Text Field}
```
Parameter | Description
- |:-
Show Visitor Count Popup | Show Visitor Count Popup when Customer visit Product page
Remove cookie every | Time gap to **Reset** the number of visitors show in the product. <br>
Counter Min | Minimum visitor number to show
Counter Max | Maxium visitor number to show
Value range of counter after reload page </br> (Percent x The previous value) | Standard deviation range when visitor refresh product page in Web Browser.
Popup position | Popup position display in Desktop
Icon User | Icon image User icon
Text Field | text show at {Text Field}
Background color | Visitor Count Popup background color

### Discount Popup

![](/discount_coupon_popup.png)

Configure to Product Popup Coupon. Intent Exit Popup when customer **hover out of Product page**.

## 7. Product Card

![](/product_cart_auto.png)
[<button>Go to Product Card Configuration</button>](/product-templates.html#configure-variant-color-swatch)
## 8. Swatch Styles
![](/swatch_style.png)
[<button>Go to Swatch Style Configuration</button>](/product-templates.html#configure-variant-color-swatch)