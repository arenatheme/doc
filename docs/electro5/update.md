---
metaTitle: Electro - Theme Update Tutorial - ArenaCommerce Help Center
sidebarDepth: 2
---  

# How to Update your theme  

The theme update process will differ by the methods you chose to install your theme. Please choose the methods listed below to see the details.  
:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Arena Dashboard" id="arena-dashboard-update"
You will get notify when we have new update for any licensed theme you added. Flowing process as Install.
:::
::: tab "Manually" id="manual-update"
When you manually add an updated version of a theme, a second version of the theme is added to your online store. The new version is in its default state, with **none of your customization applied**. The older version is unaffected by the update, and still has all of your code customizations.  
1. [Create a backup copy of your existing theme.](https://help.shopify.com/en/manual/using-themes/managing-themes/duplicating-themes)
2. Download the new theme file from ThemeForest or install our Arena Installation App.
3. Install the new theme version as new theme for your store.  
![](/electro6/electro-6-upgrade-1.gif)  
4. Copy your current settings_data.json file to new theme's package  
![](/electro6/electro-6-upgrade-2.gif)  
5. Move custom .CSS and .JS if you have them.
:::
::::  