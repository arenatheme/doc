---
metaTitle: Configure Product Pages Shopify Theme Electro 5.0
sidebarDepth: 2
---
# Electro Products
::: danger
Before configuare the following page, you must **Publish** the Electro theme
    ![](/electro5/publish.png)
:::

## Product Templates
Product Template Name|Template Description
:-|:-
product| product template use as default when create a new product
^^|[<button>Demo Product Simple</button>](https://electro.arenacommerce.com/products/consectetur-nibh-eget)
product.bundle|Product templates support product bundle.
^^|[<button>Demo Product Bundle</button>](https://electro.arenacommerce.com/products/gold-diamond-chain)
product.extended|simple product template added more functions
^^|[<button>Demo Product Extended</button>](https://electro.arenacommerce.com/products/quisque-placerat-libero)
product.redirect|product template replace Add to cart button by Redirect Url button
^^|[<button>Demo Product Redirect</button>](https://electro.arenacommerce.com/products/condimentum-turpis)


### Product Default Template
If you would like to use alternative product template as default template for product page. You may follow:

1. Go to `Online Stores` > `Themes` > `Electro Theme` > `Customize` > `Theme Settings` > `General Settings` > `Product Page`> `Template default`.

2. Select template you would like to use as Product default template
![](/electro5/product_select_default.png)

### Simple Product Template
It's only avaiblable when `Default template` is `Simple`
    ![](/electro5/product_template_set_simple.png)

1. [Create new product](../customization/product.md#creating-product)
2. Select template `product`
3. Add Products Advantage Contents by `Meta Field`
4. Save product
5. Customize Simple Product Template by Section

#### Simple Product Section Configuration

::: theo
This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Electro Theme` > `Customize` >  `Sections` > `Product page default`
![](/electro5/product_section_settings.png)
:::
<!-- ![](/electro5/product_simple_section.png) -->
![](/electro5/product_section_setting_list.png)


### Product Bundle Template
1. [Create new product](../customization/product.md#creating-product)
2. Select template `product.bundle`
3. Add Product detail & Custom Content by `Meta Field`
::: right
[Add Custom Product Meta Field by Custom Field or Bulk Editing](/installation/manual.md#using-custom-fields-for-shopify)
:::
4. Save product
5. Customize Simple Product Template by Section

### Product Extended Template
Similar to Product Simple
### Product Redirect Template
1. [Create new product](../customization/product.md#creating-product)
2. Select template `product.bundle`
3. Add Product detail & Custom Content by `Meta Field`
4. Save product
5. Customize Product Redirect Template by Section

## Configure Product Tabs
Verify that: 
- [x] Show product tabs: Enable

### 1. Add Product Description Tab

* Navigate to CONTENT tab. Click **Add content**, select **Tab - Description**
* Click **Save**.

### 2. Add Product Static Description Tab

Static tab are created base on Your Shopify page Content. It's usefull for some content that is same for all the products like Shipping term, Size Guide, and so on

1. Navigate to CONTENT tab. Click **Add content**, select **Static Page**.
2. Modify **Heading** and **select Page** to use.
3. Click **Save**.
4. New Description tab with display with Heading name and content of the tab is the page you selected.

### 3. Add Product Dynamic Description Tab

Standard tab are created base on your **Product description content**. If you'd like **unique set of tabs for each product** you shoud use heading 5 function. It's require to show your product description. You need to use it if you want to display the description of the product.

1. Navigate to CONTENT tab. Click **Add content**, select **Standard Description**
2. Click check **Use Multi-tabs with heading 5 tag** to active **dynamic heading description tab for each product.**.
3. Click **Save**
4. Now go to your produc and edit the description product and adding your unique tab for each product using
```
<h5>Tabs Title</h5>.
```
![](/electro5/product_dynamic_tab.png)


## Configure Variant Color Swatch
![](/electro5/product_swatch_style.png)

::: theo
This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Electro Theme` > `Customize` >  `Theme Settings` > `Swatch Styles` 
:::
1. [Add variants while you're creating a product](https://help.shopify.com/en/manual/products/variants/add-variants)

2. Configure Swatch Options:

    - Button: Enter Option name into enable Text/Button Swatches For Variable Product Attribute Variations. Each option name seperate by comma. E.g. Color, Size, Material
![](/electro5/product_swatch_button.png)

    - Color: Enter Option name into enable Color Swatches For Variable Product Attribute Variations. Each option name seperate by comma. E.g. Color, Style

    - Image: Enter Option name into Enable Image Swatches For Variable Product Attribute Variations. Your Product variant image will show up instead of Color. Each option name seperate by comma. E.g. Color, Style.

    ::: right
    [Add image to product variants](https://help.shopify.com/en/manual/products/product-variant-images#add-images-to-existing-variants)
    :::
  

3. Add Color Code or Color Image
For the color options you have, you can either let the smartness of the script provide a color for you, or you can upload an image that represents that color.  
    - Color Code: Enter color code if it's exits hex code.
    ![](/electro5/product_swatch_color_code.png)

    ```
    e.g. red: #FF0000,pink: #FFC0CB,black: #000000,grey: ##808080,brown:#A52A2A,blue:#0000FF,gold:#FFD700,white:#FFFFFF,yellow:#FFFF00
    ```
    - Upload your color images to Theme assets
    ::: theo
    The way I have gotten my images from my products in my demo shop was to open up each product image on my storefront in the “colorbox” \(lightbox\) and grab around ~ 40 by 40 pixels section of the product image, then save that small screen grab to my desktop, and rename the image.

    There's an important naming convention to respect here! The image must be named after the color option, but be handleized, and have a .png extension.

    For example, if you have a color called 'Déjà Vu Blue', then name your image `deja-vu-blue.png`
    #### Steps
    Other example, if your color is 'Blue/Gray', then name your image blue-gray.png.

    1. Most simple example, if your color is 'Black', the name your image black.png.

    2. From your Shopify admin, go to **Online Store &gt; Themes.**

    3. Find the theme you want to edit, and then click **Actions &gt; Edit code.**

    4. On the **Edit HTML/CSS page**, locate and click on the **Assets** folder to reveal its content.

    5. Under the **Assets** heading, click on the **Add** a new asset link.

    6. **Upload your image.**

    :::


4. Configure Swatch Sharp. You may select to change button/color/image swatch to Square or Cirle (Rounded).
5. Configure Swatch Size.
## Configure Sidebar
## Popup Discount Modal Configuration
## Add Custom Field to Product