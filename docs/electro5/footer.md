---
metaTitle: Configure Header Section Shopify Theme Electro 5.0
---

# Electro Footer Section

::: theo
Refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Electro Theme` > `Customize` >  `Sections` > `Footer`
:::
## Configuration
![](/electro5/footer_section.png)

## Content
Combinate cotent and you will get more and more footer designs. Please remember to check your total column width settings must equal 12.

![](/electro5/footer_content.png)

