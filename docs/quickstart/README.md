---
sidebarDepth: 3
metaTitle: General Support Information - ArenaCommerce Help Center
---
# 1. General Support Information

## 1.1 Theme support  
Please read this document carefully, it will help you eliminate most potential problems regarding incorrect configurations of the theme and Shopify.  
If you don't find the answer to your questions, please refer to [this chapter](/quickstart/support.md) for more information about our support policy.  

## 1.2 Shopify support  
Shopify related configuration, installation, maintenance, customization, etc... is beyond the scope of our theme support.  
::: warning
If you are setting up a Shopify store for the first time, we recommend you familiarize yourself with the [Shopify Help Center](https://help.shopify.com/en) for a full overview of the platform.
:::  

## 1.3 Free Shopify Installation Service
If you have no prior experience with setting up an online store, we still provide **free installation service** to help you warm up with our Shopify theme. For complete user manual, please choose the specific theme you bought from the `Theme Customization` tab dropdown at the upper right corner of this site.  

[<button class="btn"><i class="far fa-hand-point-right"></i> Request Shopify Installation Service</button>](https://www.arenacommerce.com/pages/free-installation-service)  

You will get:  
- **Free Development Store**: No need to pay for time you spend testing and configuring the store (limited to 50 customer orders/ store only). Transfer ownership when the store is ready.  
- **Free Theme Installation Service**: If you have our themes license, we offer free theme installation & import the demo store as seen on LivPreviews for you (including mega menu).  
**Why Free?**  
Since you pay Shopify for your account, Shopify will then pays us a commission. If you get your site setup for free, Shopify gets another happy customer and we get a monthly commission based on your Shopify plan **(not your sales)**. You pay us nothing but all of us win!  

## 1.4 Our support schedule  
- Our timezone is GMT+7 and working hours is from 9AM to 6PM from Monday to Friday.    
- We do our best to monitor your queries (refer to as tickets) around the clock. Reply times can vary from time to time and be up to **24 hours from Monday – Friday**, and up to **48 hours on weekends** due to different timezones.
- Please make sure you have read all the available help documents before [submitting a support request](https://arenathemes.freshdesk.com/support/home).  
- Our holidays day-offs follows local laws, so we are available on most Black Fridays, ThanksGiving, Christmas, etc... for your emergency requests. We will send a notice to you 1 week prior to our state's holiday.  
- We highly recommend coming to us with your theme-related issues. However, if you do not feel comfortable sharing access to your website with our staff members, we recommend hiring a freelance developer, whom you can collaborate closely with, to help with your website issues. Your private developer can contact us any time for support.  

## 1.5 Third-party apps  
- Because Shopify apps are developed by third party developers, your best bet is to check with the app developer to see if their app is compatible with our themes.  
- Please note that we are unable to modify our code to make our themes compatible with any app because resolving one issue for one app often introduces errors in other theme functions or other apps.  
- Instead, we do offer app developers access to our code so they can make the necessary changes on their end to make their app work with our themes. If your app developer is interested in this program, [please have them contact us for details](https://www.arenacommerce.com/pages/partner).  
- We keep a curated list of apps whose developers have agreed to keep their app updated to function with the updated versions of our themes. Please send your queries to [our Services page](https://arenathemes.freshdesk.com/support/home) for more details.  