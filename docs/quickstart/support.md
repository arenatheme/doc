---
metaTitle: Support Policy - ArenaCommerce Help Center
sidebarDepth: 3
next: /installation/
---

# 3. Support Policy
## 3.1. Support  

::: warning Please note that as authors of the theme
We are able to provide support only for the features which we created and for the issues related strictly to the theme.
:::  
- Many support queries and technical questions have already be answered in supporting documentation on this site and [our Support Center](https://support.arenacommerce.com), along with comments from previous buyers on Envato Market. We also make sure that all the items we sell on the Envato Market platform have documentation included so we recommended you to always check the included documents first. If you can’t find the answer you’re looking for, then, consider sending us a support request [here](https://arenathemes.freshdesk.com/support/tickets/new).  

- Since our themes are considered items on the Envato Market platform, buying them means you and us are essentially bound by their [Item Support Policy](https://themeforest.net/page/item_support_policy). Our team at Arena guarantee to fulfill our end according to their terms and conditions as listed from this point onwards. You can also click on the link in this paragraph to go to the Envato Item Support Policy page for further details.  


## 3.2 Item Support Policy  
### 3.2.1 What is included in item support?  
#### The item support period  
- A supported item includes item support for 6 months from the purchase date. During those 6 months, the author is expected to be available to provide the item support services we’ve set out on this page. Response times by authors can vary depending on the volume of inquiries, the nature of the request, and whether questions have already been answered or support has already been provided (see ‘fair-use’ later on).  

- If you’re about to purchase the item, you’ll have the option to purchase extended item support, increasing the item support period up to a maximum of 12 months from the date of purchase.  

#### Answering questions about how to use the item  
- During the item support period, the author is expected to be available to answer your general questions about the item and how to use it. For example, how do I get my homepage to look like the one in the preview? The response to this type of question can come in various formats including directing you to an already documented response (e.g. in the comments or FAQs).  

#### Answering technical questions about the item (and included third-party assets)  
During the item support period the author is expected to be available to:
- Answer your specific questions about the features and functionality of the item.  
- Provide some guidance on the way the item is designed.  
- Help you with issues related to using the item and getting the most value out of its functionality.  
- Answer questions about third-party assets or functionality (e.g. plug-ins) bundled with the item, such as how they work and other technical questions.  

#### Help with defects in the item or included third-party assets  
- During the item support period, you can report and discuss bugs and minor item defects with the author, and authors are expected to be available to assist you with reported bugs. If appropriate, authors may issue bug fixes directly to you as part of item support. (If an author decides to address a bug fix through a general version update, that update will be available to all buyers.)  
- A supported item may include third-party functionality or items from other authors such as plugins, image sliders or contact forms. During the item support period, the author is expected to be available to assist with questions about third-party assets, and to either help you to address particular issues with the third party asset or direct you to where you can find the solution.

#### Item updates to ensure ongoing compatibility and to resolve security vulnerabilities  
- If a supported item includes a third party asset (e.g., a plugin), or is intended to work with third-party software or platforms (e.g. a CMS), authors are expected to ensure the item remains compatible if the software or platform version is updated. This includes if the third party asset has a security update. Item updates of this type will generally be delivered as version updates available to all buyers. Choosing a supported item (or upgrading to extended support) ensures you’re getting an item that’s backed by the author and enables our authors to maintain their items in these ways.


### 3.2.2 What's not included in item support  
#### Item customization  
- Item support does not include services to modify or extend the item beyond the original features, style, and functionality described on the item page. For customization services that will help you tailor the item to your specific requirements, we recommend contacting the author to see if they privately offer paid customization services or check out the great service providers on Envato Studio.

#### Installation of the item    
- Buyers have varying levels of experience with different software platforms and technologies and getting your item up and running can depend on many variables that are specific to your circumstances. You’ll need to have a working knowledge of the software platforms and technologies for which items are created, so item support does not include help to install the item on your server or on a CMS. If you’re just starting out or skilling up, try a course or tutorial on Envato Tuts+. If you’re after installation services, you can ask the authors if they privately offer paid installation services or you can check out service providers on Envato Studio.

#### Hosting, server environment, or software  
- Authors are not required to support issues about your web hosting or server environment, or issues with the software you’ve got installed on your machine to use the item. Please check your ISP/web hosting provider or other software documentation that you’re trying to use to help solve your issues.

#### Help from authors of included third-party assets  
- If a supported item includes a third party asset (e.g. a plugin or slider), the author of the third party asset is not required to provide support for that asset to buyers of the supported item. Direct your questions about third party assets to the author of the item you purchased.


### 3.2.3 What’s included in all item purchases  
#### Updates to ensure the item works as described and is protected against major security concerns  
- All items on Envato Market should work as described by the author (in the preview, screenshots, item description, etc) and be protected from major security issues. Security issues are assessed by the impact they have on the buyer and their users. Examples of major security issues are site administration takeover, the ability to use the site against other sites, and breaches of private data.  
- In the instance that there is something wrong with the item and you are expecting an update; depending on the type of update, it can take several days/weeks to properly fix, test, review, and release.

#### Included version updates for all items  
- In addition to updating an item to keep it working as described or to fix major security issues, authors of all items may from time to time and at their discretion, provide other updates to improve or modify the functionality of an item or fix other minor issues. Generally, authors of supported items will provide these type of updates more frequently to keep their items up to date for sale. In all instances, the new version of the item will automatically be made available to previous buyers through the Downloads page.

### 3.2.4 Fair-use, and other details  
We believe an author’s time is best spent using their amazing creative and technical skills to build great new items and keep their existing items up-to-date. We have outlined what item support is all about and we ask that you’re mindful of the author’s time when making item support requests. We suggest that before you ask an author for support, read the documentation (included with the downloaded item) and any additional information available on the item pages (FAQs, Comments, etc) to see if that information answers your questions.

Most authors sell their items to many buyers, so the time taken to respond to your request can vary. If you’re waiting for an update or fix to an item, it may take several days/weeks for an author to properly fix, test, review, and release. Authors can also take breaks (e.g. a vacation) from providing item support. Authors will let you know of any extended breaks via the Support and Comments tabs on their items.  

***Version 1.0 - Effective date: September 1, 2015.***  

::: right
Source: [Envato](https://themeforest.net/page/item_support_policy)
:::  