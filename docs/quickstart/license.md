---
metaTitle: Shopify Theme License Policy - ArenaCommerce Help Center
---
# 2. License

With ThemeForest regular license, you are licensed to use one of our theme to create one
single End Product (the final website customized with your content) for yourself or for one
client.

## What is allowed with single regular license  
1. You can create one single website for yourself or for your client and you can transfer that
single website to your client for any fee. This license is then transferred to your
client.
2. To sell the same website to another client, you will need to **purchase another
regular** license.
3. You can install the theme on your development store for testing/development purposes and that installation can never be available to the public.
4. You can modify or manipulate the themeand myabe combine it with others works to
create the End Product.
5. Theme updates for that single End Product (website) are free.

## What is never allowed  
1. With a single regular license you are never allowed to create multiple websites. For
multiple websites you will need multiple regular licenses.
2. Multi-store Shopify installations (incase you use multi domain for multi language store) are never allowed with single regular license. Each regular license entitles you to use the theme in only one store
(store/domain/subdomain). **For multiple stores/domains/subdomains you will need multiple regular licenses.**  

:::tip For more details, please visit ThemeForest extended license information below  
 - License FAQ: [http://themeforest.net/licenses/faq](http://themeforest.net/licenses/faq)
 - License comparison table: [http://themeforest.net/licenses](http://themeforest.net/licenses)
 - Regular license details: [http://themeforest.net/licenses/regular](http://themeforest.net/licenses/regular)
 - Extended license details: [http://themeforest.net/licenses/extended](http://themeforest.net/licenses/extended)
 :::  
