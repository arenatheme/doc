# Zeexo Changelog

## 1.2 - 14 April 2020
- New: Add 2 more header styles
    - Accesories with 5 more skins
    - Carpart: 6 skins
- Improved: Mulit-currency support Shopify Payment - Checkout
## 1.1 - 20 March 2020
- New: Loox Review App Integration
- New: Instant Layout Options
- New: Font update for selected skins
- New: Badge Styles
- New: Video guide included inside Theme Editor
- New: Inner pages sections for Selected Skin
- New: Mega menu design for selected skin
- New: More clear & stable theme management
- Improved: Theme Speed

## 1.0 - 20 January 2020
- Initial Release
