# Zeexo Guide: Home Page Sections

## Banner & Text
@[youtube](BzpzUD1FYz4)
## Logo list
@[youtube](s8uZ-RmhHdI)

## Lookbook
@[youtube](764OBhc3N5c )

## Lookbook V1
@[youtube](s8uZ-RmhHdI  )

## Product Grid
@[youtube](Vr_DfZXfPgo )

## Product Grid V2
@[youtube](ecnYoUaAD8k )

## Product Grid V3
@[youtube](9jLW6Xmutjk)

## Product Grid V4
@[youtube](h4Xh9SWSCUU)

## Product Grid V5
@[youtube](eDEG1WXOsto )

## Product Listing
@[youtube](pLF0bcnsElM )

## Product Tabs
@[youtube](F1_D2C7VAYk)


## List Collection V1
@[youtube](QZD_3qGItdY)

## List Collection V2
@[youtube](ic5z5RtmOdA)

## List Collection V3
@[youtube](YACoexYjdHw)


## Custom Content
@[youtube](5qFG-eIri6I)

## Blog List
@[youtube](1eLDkUq771w)


## Homepage Left Column
@[youtube](vYVPzbTtN2c )

## Menu List
@[youtube](eSrEvOJmJ6g)


## Service Bar
@[youtube](qRpWQ2qEW2Y)

## Announcement Bar Sections
@[youtube](plTG22L55Wg)
