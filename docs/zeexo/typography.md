# Zeexo Guide: Typography

Like other Shopify themes, the Zeexo theme will include typography settings within the Theme Editor (customize) under **Theme Settings** > **Typography**. These settings will apply theme wide to your body text, headings, sub-headings etc.

In addition the those, the Zeexo theme also includes a couple of **per section typography settings**. This means that in some of Zeexo's sections and features, you can **update the typography for that specific area** instead of the default typography settings getting applied.

## Typography Theme Settings
If you head into **Customize > Theme Settings > Typography**, you'll see the following settings available:

![typography](/zeexo/typography.png)

Within the settings for the theme's Heading & Body text, you can select a font using Shopify's Font Picker or Google Font and determine the base size.

![typography](/zeexo/zeexo-typography.gif)
