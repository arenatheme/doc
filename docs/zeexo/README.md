---
metaTitle: Zeexo 1.0 Shopify Theme User Guide - ArenaCommerce
sidebarDepth: 0
---
# Get Started Quickly with the Zeexo Demo Shop Styles

[<button class="btn"><i class="far fa-arrow-alt-circle-right"></i>Preparing Your Shopify Store</button>](/installation/#preparing-your-shopify-store)

[<button class="btn"><i class="far fa-arrow-alt-circle-right"></i>How to install theme with Arena Dashboard</button>](/installation/dashboard.html)

The Zeexo theme takes "theme styles" a step further by allowing you to import the styles — in full. This means that you can import all of the settings, content, sections, products, collections, placeholder images and even navigation (included mega navigation) as seen in the Zeexo demo shops into your own shop.

### When to use a Xstore demo shop as a starting point:

- You have a website in mind that you want to copy.
- You need to get your shop live ASAP!

### The main factors that you should consider when choosing a demo shop:
- Choose the demo that looks most like the layout you'd like your shop to have.
- Remember that you can **mix + match everything you see in the demos**

### Tips on choosing which demo shop to start with:


1. The main factors that you should consider when choosing a demo shop: 

If your industry is already represented by one of the shops, choose that one so that you'll be able to quickly populate your shop with relevant images/text (you can always change colors + layout but you'll find it most helpful to already have shop-appropriate content built right in from the beginning!)

In order to select a demo shop style to start, you can either take advantage of the **Arena Dashboard App**

@[youtube](YW_vW5Nl_N0)


## Choosing a Demo Shop to Start Out With

![](/select_v1.png)
![](/select_v2.png)

::: danger
We recommended you **[**open new Shopify**](https://www.shopify.com/?ref=supportcenter)** when you decine importing demo styles belong difference version that we listed above because when you import sample data it's may conflict & duplicate data when import from **Arena Dashboard**.

**Eg. Import Fashion 1 & Furniture 1**

If you import another demo style from similar sample style you must SKIP import data step in Arena Dashboard if you already did it before.
![](/arena_dashboard_skip.png)

**Eg. Import Fashion 2 & Fashion 1**

:::
Picking one that looks most like the layout you'd like your shop to have is typically the best route to go.

Although it's only one theme package which mean we can change theme settings to make the basic one into 100 demos store we would separate the the theme into some several theme version in order to fit sample demostration content fit the layout:

- Fashion
- Fashion Modern
- Fashion Clean
- Fashion Jewelry
- Fashion Simple
- Furniture
- Glasses
- Market
- Tools Organge
- Comestic



