---
metaTitle: Cart Page Settings Chromium 2.0 Shopify Theme
sidebarDepth: 2
---
# Chromium Cart Page

::: theo
Refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Chromium 2.0 Themes` > `Customize` >  `Sections`.
  
Select your Cart page from the drop-down page selector `Cart`
:::

![](/chromium/cart_settings.png)

## Configuration

![Cart Page Configuaration](/chromium/cart_page.png)

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "1. Cart Layout" id="cart-layout"
- **Layout Boxed**: change layout from Wide to Boxed
- **Cart Background Color**: change background color cart page
:::
::: tab "2. Cart Order Notes" id="chromium-cart-notes"
- **Enable Cart Order notes** - Check it to show Cart Order Notes for customer
:::
::: tab "3. Terms & Conditions" id="chromium-cart-term"
- **Enable term & condions**: Check it to Show term & conditions checkbox at cart page. Customer should check this checkbox to payment (agreed with your term)
- **Image**: Upload image to show your payment cert or other purpose.
:::
::: tab "4. Shipping Calculator" id="chromium-shipping-calculator"
The shipping rates calculator displays your shipping rates on the cart page of your store. If a customer is logged in, then the calculator uses the customer's default shipping address to estimate shipping rates. The shipping rates calculator works with carrier-calculated rates, manual rates, or a combination of the two.
- **Show the shipping calculator?** - set this to **Yes** to display the shipping rates calculator on your cart page, or **No** to hide it
- **Default country selection** - choose which country will be selected by default
:::
::: tab "5. Upsell Products" id="chromium-cart-upsell"
- **Enable Upsell Products"** - check it to show upsell products when customer add any products to cart.
- **How to select upsell products** - Select method to recommend products in cart
- **Upsell Heading** - enter the text that will be displayed above your Upsell products list
::::
