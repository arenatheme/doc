---
metaTitle: Configure Product Pages Shopify Theme Chromium 2.0
sidebarDepth: 2
---
# Chromium Products

The product page showcases a product and its variants, and includes product images, pricing information, a product description, and an Add to cart button. Every product that you have specified in the admin to be available on your online store has its own product page on your website. There are many ways that you can customize the layout, style, and behavior of your product pages.

For more detail about product page you may visit [https://help.shopify.com/en/manual/products/understanding-products](https://help.shopify.com/en/manual/products/understanding-products)


### Customize product template with sections

There are 4 types of collection template in Chromium:
- product: https://arena-chromium.myshopify.com/products/black-fashion-zapda-shoes
- product.bundle: https://arena-chromium.myshopify.com/products/consectetur-nibh-eget
- product.redirect: https://arena-chromium.myshopify.com/products/daltex-product-example
- product.pre-order: https://arena-chromium.myshopify.com/products/donkix-product-sample?variant=14557183246434

In order to select a template for your product, at the very bottom of the most right column of your product admin window, please find section 'Theme templates'
![](/chromium/Screenshot_product-template.png)

They use static sections for configuration so depending on which template you choose, there will be a corresponding static section.

From your Shopify admin, go to **Online Store &gt; Themes**

* Find the theme that you want to edit and click **Customize**

* From the top bar drop-down menu, select the type of page that you want to edit from the top bar drop-down menu.
* Select **Product pages.** Now you will access to edit Sections for Product page.
However, Shopify only support for only **1 product page template** to customize at select tab.

> You can change product page template view by add
> > ?view=**product.templatename**.

> at the end of product url which redirect is product template name.
> Chromium support 4 more product templates named \(**redirect, bundle, extended **and** pre-order**\). The default template can switch with no view name \(?view=\).
> Eg.
>
> * [https://arena-chromium.myshopify.com/products/black-fashion-zapda-shoes\*\*?view=bundle\*\*](https://arena-chromium.myshopify.com/products/black-fashion-zapda-shoes**?view=bundle**)

* Add link text **?view=templatename** after /editor in the URL ** **of step 2** and click **Enter\*\* to refresh and customize another page template section.

Eg. [https://shopify.com/admin/themes/38768541794/editor\#/products/black-fashion-zapda-shoes](https://shopify.com/admin/themes/38768541794/editor#/products/black-fashion-zapda-shoes)

To edit product bundle template
[https://shopify.com/admin/themes/38768541794/editor?view=bundle\#/products/black-fashion-zapda-shoes](https://shopify.com/admin/themes/38768541794/editor?view=bundle#/products/black-fashion-zapda-shoes)

To edit product pre order template
https://shopify.com/admin/themes/38768541794/editor?view=pre_order\#/products/black-fashion-zapda-shoes

To edit product redirect template
https://shopify.com/admin/themes/38768541794/editor?view=redirect\#/products/black-fashion-zapda-shoes

To edit product default template
https://shopify.com/admin/themes/38768541794/editor?view=#/products/black-fashion-zapda-shoes

* Click to Static Sections at the left side to customize your product page.
![](/chromium/chromium-product-section.png)

### Related topics

* [Product Color or Image Variants](/products/product-color-variant.md)
* [Product Size Variant](/products/product-quantity-selector.md)
* [Pro Multi-Tab Description](/products/product-multi-tab-description.md)
* [Bundle Product Page](/products/bundle-product-page.md)
* [Short Description](/products/countdown-product.md)
* [Countdown Timer](/products/count-down-product.md)
* [Exit Intent Popup Coupon](/extensions/exit-intent-popup.md)
* [Redirect Product Page](/products/redirect-product-page.md)


