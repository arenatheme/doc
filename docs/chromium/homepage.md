---
metaTitle: Customize Theme Section with Chromium 2.0 Shopify Theme
sidebarDepth: 2
---

# Chromium Homepage Sections
<!-- ## Slideshow V1

::: theo
This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Chromium 2.0 Themes` > `Customize` >  `Sections` > `Add Section` > `Slideshow V1`
:::
Slideshow under Verticle Mega Menu
- Layout
- Auto Slides: Configure to show auto slideshow switch
    - Auto rotate between slides: Enable auto swith slide
    - Change slides every: configure time switch
    - Slideshow Animation: Switch slide animation effect
::: tip
Click `Save` top reload page when see crash
:::
- Navigator & block three images
    - You may select image block at style 1 (with Padding) & style 2 (without padding)

### Add Slideshow
Click `Add Content` and select slide type:
- Slideshow - Image
    - Background image on desktop 
    - Background image on mobile
    - Position a background-image
    - Slideshow link to: setting link to connect when click to the slideshow background image
    - Image layout
    - Text box layout & style

- Slideshow - Video

- In order to embed video from Youtube please select option: 
    - [x] Embed video
and insert your Youtube embed code into Video url
- If you would like to run video player Go to `Settings` > `Files` > click `Upload files` and upload your video (major video file types such as .mp4 and .mov), then copy the url and paste to `Video url`

::: warning
You can't upload video files when you're on a trial membership Shopify. In order to upload video file you may try our Developer Store with free to upload.   [Register here:](https://www.arenacommerce.com/pages/free-installation-service)
Each file needs to be smaller than 20 MB to upload to Shopify.

:::

- Block Content
## Slideshow V2

::: theo
This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Chromium 2.0 Themes` > `Customize` >  `Sections` > `Add Section` > `Slideshow V2`
:::

Similar to Slideshow V1, you may configure the options following:

## Image Gallery V1

::: theo
This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Chromium 2.0 Themes` > `Customize` >  `Sections` > `Add Section` > `Image Gallery V1`
:::
 -->
Updating