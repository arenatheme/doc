# Chromium Blogs

## Blog Page Configuration

This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Chromium 2.0 Themes` > `Customize` > `Shopify menu dropdown` > `Blog posts` > `Blog pages`


## Blog Article Configuration

Refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Chromium 2.0 Themes` > `Customize` > `Shopify menu dropdown` > `Blog posts` > `Article pages`
