---
metaTitle: Configure Header Section Shopify Theme Chromium 2.0
---

# Chromium Footer Section

::: theo
Refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Chromium 2.0 Themes` > `Customize` >  `Sections` > `Footer`
:::
## Configuration

## Content
Combinate cotent and you will get more and more footer designs. Please remember to check your total column width settings must equal 12.

