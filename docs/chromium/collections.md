---
metaTitle: Product Collection Configuration with Chromium 2.0 Shopify Theme
sidebarDepth: 2
---
# Chromium Collections
<!-- ## Collection Templates


Collection Template Name|Template Description
:-|:-
collection|Collection page with default pagination
collection.infinite|Collection page with infinite scroll pagination


## Collection List Configuration

::: theo
This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Chromium 2.0 Themes` > `Customize` >  `Collection list`> `Sections` >`Collections list page`
:::


## Collection Pages Configuration

::: theo
This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Chromium 2.0 Themes` > `Customize` >  `Collection pages` > `Sections` > `Collection pages`
:::
### 1. General Layout

Parameter|Description
:-|:-
Breadcrumb image| Image for Breakcrumb background
Position of the collection description|Top: Show at top collection body
^^|Bottom: Show at bottom collection body
^^|None: Hide collection description
^^|![Electro 5 Collection Description](/electro5/collection_description.png)
Enable Mode View| Show mode view
Enable Sort by| Show Short by
Products per page| How may products show in a collection page
### 2. Collection Filter

Parameter|Description
:-|:-
Disable "AND" operator in group filter| Remove and in group filter
Filter type|Filter by Tags: Use tags to filter product
^^| Filter by Groups Tags - [How to setup Group Filter Tag](https://arenathemes.freshdesk.com/support/solutions/articles/6000177980-how-to-setup-shopify-group-filter-tags-arenathemes)
^^|^^
Filter position|Sidebar: Show Collection Filter in Sidebar
^^|Body: Show Collection Filter in Collection Body Content
^^|None: Disable Filter
Content| Add Content to show Filter
^^ | Heading Filter
^^ | Filter by Groups

### 3. Collection Sidebar Mode
Parameter|Description
:-|:-
Sidebar Mode| Left Sidebar: Display Sidebar content in Left Side
^^| Right Sidebar: Display Sidebar content in Right Side
^^| None: Hide Sidebar Content
Sidebar Content| Filter Content If select to show filter in Sidebar
^^ | Sidebar - Categories
^^ | Sidebar - Product
### 4. Add Collection Pages Content
Click `Add Content` to add advantage content block to Filter & Body Collection. It should be included:

#### 4.1. Sub Collection

1. Create nestest menu to control hierarchy
2. Add Sub-collection content and Assign nestest menu to Sub-Collection content.

::: right
[Create a Nestest menu Official Documentation](https://help.shopify.com/en/themes/development/building-nested-navigation)
:::
#### 4.2. Heading Filter

Add a Filter Heading to Filter Side

#### 4.3.Filter by Groups

Add a Group Filter Block by enter product tag to filter in `Product tags` field . It's flexible for all store that you can select any tag or tag group to display in `Filter`.

- You can drap & drop arrange Group Filter Content to change the order display

##### How Collection Filter Work

- To each filter group, there are many values. For instance, Color has its value: Black, Blue, Red, White, Yello
- When a value is selected, for instance "green", all products (in the collection) having "green" tag will be displayed.
- **The filter Color is displayed in swatches by default.**

::: theo Add Product tags
Go to Products > Products, choose a product and go to `Tags` Area
:::

#### 4.4. Sidebar - Categories

- You may show all (no limit) or selected Collection (max 12 collections) in Sidebar 
- or Show a Collection Nestest Navigation Menu item in Sidebar with support Collection Child with active links. Eg [Check this link](https://electro.arenacommerce.com/collections/music)
::: right
[<button class = "markdown-button" name="button">Create Nested Menu items - Official Document</button>](https://help.shopify.com/en/themes/development/building-nested-navigation)
:::
- Display thumbnail icon beside Collection name

Parameter|Description
:-|:-
Heading| Heading Label
Select collections to show| Navigation: Show a Nested Navigation Menus
^^|All: Show all collection you has been created
^^| Selected: Show selected collection below only
Thumbnail icon| Use collection image: show collection image as thumbnail
^^| Upload from assets (png format): Upload your image with name similar to collection hande.<br> e.g Collection name/handle: `All in one` -> filename: `all-in-one.png`
^^| Upload from assets (svg format): Upload your image with name similar to collection hande.<br> e.g Collection name/handle: `All in one` -> filename: `all-in-one.svg`
^^| Upload by Settings - Use when choose selected: Show selected image as thumbnail icon for selected collection below
^^|![](/electro5/collection_thumbnail_icon.png)
^^| None: Hide Thumbnail icon
Menu items| Select menu to display as Categories List
COLLECTION 01-12| Select collection to display as selected collection

::: theo Upload Thumbnail image to Theme Assets
Prepare collection image with size 25x25 pixels, then save my desktop, and rename the image.
There's an important naming convention to respect here! The image must be named after the color option, but be handleized, and have a .png extension.
For example, if you have a collection called 'All in one', then name your image `all-in-one.png or all-in-one.svg`
#### Steps
1. Most simple example, if your collection is 'acessories', the name your image acessories.png.
2. From your Shopify admin, go to **Online Store &gt; Themes.**
3. Find the theme you want to edit, and then click **Actions &gt; Edit code.**
4. On the **Edit HTML/CSS page**, locate and click on the **Assets** folder to reveal its content.
5. Under the **Assets** heading, click on the **Add** a new asset link.
6. **Upload your image.**
:::

#### 4.5. Sidebar Banner

#### 4.6. Sidebar Products

### 5. Product Card
::: theo Product Card Theme Settings
This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Chromium 2.0 Themes` > `Customize` > `Theme Settings >` `Product Card`
:::

Parameter|Description
:-|:-
Quick View Product| Display Quick View Product in Product Card
Show product vendor| Show Product vendor in Product Card
Show product review| Show product review from app in Product Card
Enable product's title balance | Auto balancing Product Name in Product Card
Hover effect| Effect when hover to product card
Aspect ratio | Auto scale all product image into 1 ratio.
Auto crop image | Instead of scale image, the product image in product card will be cropped when adjust them to the same ratio.
SALE LABEL| Product Sale label configure
NEW LABEL | Product New label configure
PRODUCT COLOR AND SIZE| Change Product name, price, price in sale, sold out notice Color -->
Updating