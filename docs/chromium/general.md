---
metaTitle: Chromium 2.0 Shopify Theme General Settings
sidebarDepth: 2
---
# Chromium Theme Settings

This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Chromium 2.0` > `Customize` > `Theme Settings`

![](theme_settings.png)



## 1. Apps Intergration

Step 1: [<button class="btn">Installing Shopify Apps</button>](./app.md)  

Step 2: Configure Apps & Theme Setting`
`Online Stores` > `Themes` > `Chromium 2.0` > `Customize > Theme Settings > Apps Intergration`

### WISHLIST & COMPARE

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Theme Settings" id="theme-settings"
- [x] Enable Wishlist
- [x] Enable Compare
:::
::: tab "Arena Wishlist Compare Settings" id="wishlist-setting"
Wishlist & Compare is not offered by Shopify as a function so we have built a Free Shopify Application to assist customers.
- **Wishlist** is build by using Shopify App Proxy, it's not required to setup template page on your Shopify store. Wishlist function will launch at: https://your-shopify-url/apps/wishlist
- Compare required create and assign a separate page for the Compare page display
:::
::::

::: right
Read more: [Install Arena Wishlist Compare app](/installation/apps.md)
:::

### LANGUAGES

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Theme Settings" id="theme-settings"
- [ ] Translation Method: Google Language / Weglot
:::
::: tab "Google Translate" id="google-translate"
- [ ] Enable Language Translator: enable google translate
- [ ] Languages you wish to translate: Insert language code with comma. Check out [all languages Google translate support here](https://cloud.google.com/translate/docs/languages)
:::
::: tab "Weglot" id="weglot"
- [ ] Destination languages: Choose languages you want to translate into. Supported languages can be found [here](https://weglot.com/documentation/available-languages).
- [ ] Make Private: Only work in Dev mode
- [ ] Auto Switch: Cache switch Language
- [ ] API Key Weglot: Your Weglot API key.
- [ ] Flag Type: Flag sharp in Header
:::
::: tab "Weglot Register & Settings" id="weglot-register"
#### Weglot Free?
After signing up, you have a  10-days free trial period to test Weglot. Once the 10 days free trial ends, you have 2 possibilities:
- Your website has less than 2000 words, and you want to translate it only in one language: You can use the Free plan. The Free plan is automatically chosen at the end of the trial period, you don't need to choose it manually.
- Your website has more than 2000 words: You will need to upgrade your plan to continue using Weglot.
You can find your number of translated words in your account, on your dashboard.
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/5d0a37de2c7d3a1cad5b8f13/file-VIzht1Oy3D.png)
Detailed pricing:
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/5d0a389b2c7d3a1cad5b8f21/file-uJgnrENCUU.png)
#### Register Weglot with 20% Discount <Badge text="Discount 20%"/>

- From your Shopify admin, go to `Apps > Arena Dashboard > Perk`
![](https://cdn2.shopify.com/s/files/1/0208/3880/9664/files/dashboard_large.png?v=1563251258)
- Hover to Weglot & Click **Claim Perk** to get promotion code (discount 20%) & go to register website Weglot.
Please save Promo code & you can redeem it later when you want to upgrade the service.
- Enter `your email`, `password`,  check `I agree to Weglot's Terms of Service` and click **Start Free Trial**.
- Verify your email address and go to Weglot Dashboard.
- From your Weglot Dashboard, you can access the projects tab at the very top left. Once you click on it, you'll see a drop down with a green button "Create project" to add a new project (see below).From this tab, you can also select the project you want to work on.
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/59e620d22c7d3a752de12a7f/file-wWBkWK7Dom.png)
- Enter your Project name, select website technology is Shopify.
- Click Next to create Project
- You can find the different project settings detailed below on your Weglot account, under "project settings".
![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/5d0a28022c7d3a1cad5b8e1a/file-B5tX8sHUdP.png)
    - **Name and website URL**
        Give a name to your project to easily navigate between your different projects. 
        ::: theo
        ![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/59e62b6d2c7d3a752de12afc/file-KURaNthtH6.png)
        In order to get the Visual Editor working properly, please make sure to add the correct website URL in the settings.
        :::
    - **API key**: A separate unique API is created for each project.
        ![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/5900d0900428634b4a32a461/images/59e62acc2c7d3a752de12af4/file-FzX2fu2bmz.png)

:::
::::
### PRODUCT REVIEW

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Theme Settings" id="theme-settings"
Product review by:

- [ ] None: Disable / Hide product review
- [ ] Product Review: Using Shopify Product Review app
- [ ] Judge.me: Using Judge.me app
:::
::: tab "Judge.me Settings" id="judge-setting"
#### Judge.me
It's require to select install Judge.me Review Widget Installation `Manually` because we already integration into the theme code.
![](/judgeme_manual.png)
:::
::::
### SEARCH BOX
Chromium support `Instant search widget`. When you install the Smart Search app, you should disable theme default Ajax Search

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Theme Settings" id="app-integration-settings"
- [ ] Disabled default Ajax Search suggestion: select to disable default Ajax Search to use Smart Search.
:::
::: tab "Smart Search & Instant Search" id="smart-setting"
#### Smart Search & Instant Search <Badge text="Discount 20%"/>
- Go to Smart Search & Instant Search dashboard > Instant search widget
- Select `Enable instant search widget` &  `Optimize for phones and tablets` > click `Apply changes`
![](/search_instant.png)
:::
::::

## 2. Multi Currency

::: theo Preparing Multi Currency Switcher
[<button class="btn">Preparing your store for Multi Currency</button>](/installation/#preparing-your-shopify-store)
:::
```
Online Stores > Themes > Chromium 2.0 Themes > Customize > Theme Settings > Multi Currency
```
Group Parameter | Description
-|:-
Enable Multi-currency | Enable multi currency switcher
Auto Currency Switcher On Customer Location | Use **Geo IP Location** to detect your customer location and auto select the store currency
Show Currency Flags| Show currency flag in header
Flag type | Retangle/ Square/ Circle: Show country-currency flag & select sharp
Money Format | Select money format
Currencies You Wish To Support | Separate your currency codes with a space. Use the ISO 4217 3-letter currency code.
Default Currency | Your store default curency
## 3. General Settings
```
Online Stores > Themes > Chromium 2.0 Themes > Customize > Theme Settings > General Settings
```  
|| Parameter | Description
:-:|:-|:-
Enable lazy loading images || Lazy loading is technique that defers loading of non-critical resources at page load time. <br>Instead, these non-critical resources are loaded at the moment of need. <br>Where images are concerned, "non-critical" is often synonymous with "off-screen".
Loading style|| Default pre-loading style when enable lazyload
Shop Disable Ajax Cart | Disable auto update total cart price when it is changed.
^^| Enable Catalog Mode | Disable Add to cart button
^^| Enable Search Function | Hide Search bar in Header
^^| Quick View Product: Disable Quickview button in product cards.
Button Back To Top|| Enable button back to top in Desktop
Homepage| Enable Left Column Section| Setting to enable/disable Left Column sections for homepage
^^| Boxed Left Column Section | Display left column section in Container box layout (boxed layout)
^^| Enable Bottom Section| Show botton Static Section

## 4. Typography

Parameter | Description
--|:-
HEADER FONT | Configure Heading font family
Heading 1-6 Size | Configure Font Size Heading H1-H6 & font weight
BODY FONT | Configure Body font family
Body size | Configure body font size
Top Bar font size | Configure Top Bar (Announcement Bar) font size
Product item title size | Configure font size Product name in product page
Product item price size | Configure font size Product price in product page
Product title size | Configure font size Product name in Product card
Navigation size | Configure Navigation font size
Navigation dropdown size | Configure Navigation dropdown font size

## 5. Icon
```
Online Stores > Themes > Chromium 2.0 Themes > Customize > Theme Settings > Icon
```  
The icons setting use in whole store pages. You can checkout all font class here: 

Parameter | Description||
:-|:-|:-|
Favicon | Favicon of website.|**Icon type**: select `font icon` or `image`</br></br>**Font icon name**: insert icon font class name start with icon</br></br>[<button>Find Arena Font icon class name</button>](https://font.arenacommerce.com/chromium)</br></br>[<button>How to add more icons</button>](https://font.arenacommerce.com/chromium)</br></br>**Image**: upload image file</br>
Loading icon | Loading icon|^^
Phone number icon | Phone icon in header & others|^^
Account icon| icon account|^^
Seach icon| icon search|^^
Wishlist icon | icon wishlist|^^
Compare icon | icon compare|^^
Cart icon | icon for cart in header & product cards|^^
Social icon | icon for social sharing button|^^

## 5. Colors & Style

Parameter|| Description
:-|:-|:-
**LAYOUT**|Layout Mode|Configure Wide / Boxed (Container) Layout
BACKGROUND BODY| You may select background image or color for body content||
BREADCRUMB| Configure breadcrumb detail: background color/images, height||
SETTINGS COLOR|Use Color Code|Enable to define your color name with Hex code|
^^|Color Hex Code| Add color code with format "colorname:hexcode", separate with comma.
^^|^^|E.g. red: #FF0000,pink: #FFC0CB
HEADER COLOR|Top bar background color| Background color for header top
^^| Background color| Background color for header
^^| Background for the number on icons | Phone icon in header & others
^^| Shop Department background color | Background for Header department button
^^| Custom Text color| Color for header text
Body color | Main Color | Primary color use for main site
^^|Body Background| Color body background
^^|Miscellaneous Color|Text for misc detail
^^|General Text Color|Text color in dark site
^^|General Light Text Color|Text color in light site
^^|Title Text Color|General title color
^^|Link Color|General Text Links color
^^|Border Color| General boder color
^^|Ratting Empty Color|Star rating empty color
Navigation Color| Link color | Nav Text links color
^^|Menu item hover color| Color item slide when hover.
^^|Menu item hover background color| Nav background when hover color
^^|Background Color In Dropdown| Background color for dropdown nav
^^|Navigation background color|Navigation backgroud color
^^|Link Item Color In Dropdown|color item in dropdown menu|
^^|Link Item Hover Color In Dropdown|color link item in dropdown menu|
^^|Vertical Menu Color| similar above
Navigation Label Color|Text sale label color|Text color in `Sale label`
^^|Sale label background| Background color `Sale label`
^^|Text new label color|Text color in `New Label``
^^|New label background| Background color `New label`
^^|Hot label color|Text color in `Hot Label``
^^|Hot label background|Background color `Hot label`
^^|![Background](/nav_label.png)||
Footer Color|Footer Content Background|Background color
^^|Footer Text Color| Footer text color
^^|Footer Link Color| Footer text links color
^^|Footer Link Hover Color| Footer text links hover color
^^|Footer Copyright Background| Footer bottom copyright container background
^^|Footer Copyright Color| Copyright text color
Button style 01| Custom button style 01: background, text, border, hover...||
Button style 02| Custom button style 02: background, text, border, hover...||


## 6. Fomo Popups
::: tip
Fomo Popups display real-time customer activity notifications on your store. You can use it as a Social Proof Markteting Tool to Boost Conversions.
:::

### Sale Notification Popups

Popups to show the number of Sale on your Online Store to encourage customer making purchase.

#### Sale Notification Popup
![](/fomo_demo.png)

**Sale popup template:**
::: theo
Some one in `{Random City}` `{Random Text}`.  
`{Random Product in Collection}`.  
`{Random Number}` `{Random Text}`  
:::


Parameter|| Description
:-:|:-|:-
Popup Position||Popup position to display on Desktop when lauch.<br> Position: Bottom Left/Bottom Right/Top Right/Top Left.
Visibility|Show On Home Page | Show Sales Notification on Home Page
^^|Show On Product Page | Show Sales Notification on Product Page
^^|Show On Collection Page | Show Sales Notification on Collection Page
^^|Hide on Mobile Device | Hide Sales Notification on Mobile Device
{Random City}|Random City base on | **GEOIP**: Auto Dectect Visitor Country and get list all cities to use <br> **Manual Cities List**: Input optional Cities in next field <br> **Combine both list**: Mixup cities list in visitor country & manual cities list.
^^|Manual Cities List | Your manual cities list. Separate city by **;**
{Random Text}|Random Text | Random text. eg: just bought;view;search
{Random Product in Collection}|Random Product in Collection | Select product in Collection to show
{Random Number}|Random Number | Select random number to show. Separate number by **;**
{Random Text}|Random Text | Random text. eg: minutes ago;hours ago;days ago
Background color||Background color 


### Visitor Count Popups

Popup to show Random Visitor detail to Product page.
![](/number_visitor.png)
**Visitor Count Template**:

```
{Random Number} of visitor {Text Field}
```
Parameter | Description
- |:-
Show Visitor Count Popup | Show Visitor Count Popup when Customer visit Product page
Remove cookie every | Time gap to **Reset** the number of visitors show in the product. <br>
Counter Min | Minimum visitor number to show
Counter Max | Maxium visitor number to show
Value range of counter after reload page </br> (Percent x The previous value) | Standard deviation range when visitor refresh product page in Web Browser.
Popup position | Popup position display in Desktop
Icon User | Icon image User icon
Text Field | text show at {Text Field}
Background color | Visitor Count Popup background color

### Discount Popup

![](/discount_coupon_popup.png)

Configure to Product Popup Coupon. Intent Exit Popup when customer **hover out of Product page**.

## 7. EU Policy Popup
## 8. Product Page
## 9. Collection page
## 10. Cart
## 11. Product Grid
## 12. Swatch Styles

![](/swatch_style.png)
[<button>Go to Swatch Style Configuration</button>](/product-templates.html#configure-variant-color-swatch)


<!-- <!-- ## 8. Product Card

![](/product_cart_auto.png)
[<button>Go to Product Card Configuration</button>](/product-templates.html#configure-variant-color-swatch)
## 8. Swatch Styles -->
