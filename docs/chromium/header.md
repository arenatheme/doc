---
metaTitle: Chromium 2.0 Shopify Theme Header & Navigation
sidebarDepth: 3
---
# Chromium Header & Navigation
<!-- ::: theo
This chapter refers to the following section of the theme admin panel:  
`Online Stores` > `Themes` > `Chromium 2.0 Themes` > `Customize` >  `Sections` > `Header`
:::
![](/electro5/header.png)

Header is a Static Section apply settings to all pages.

## Header Configuration
Parameter | Description ||
:-|:-|:-:
|Section Layout Mode | Setting layout for Header at Boxed or Wide||
|Header Style|You may select slide from 1-7 to adjust Header style||
|^^|![](/electro5/header.png)||
Border top - Header style 3 | Add border at top header style 3||
Fix layout wide - Header style 2 | Adjust Menu to box layout in header style 2||
^^ | ![](/electro5/fix_layout_header_2.png)||
Logo Image| Image|Upload image for your website logo. We recommend you to upload 2x image size (2xCustom logo width. <br>eg. custom logo witdh = 125px -> logo width 250px )
^^|Custom logo width (in pixels)| Select your logo width show in the header
Text box| Welcome text - Header style 2| Add top left welcome text content
^^|Shipping text - Header style 1&5| Add detail in Bottom right of style 1, 5
^^|^^|![](/electro5/header_1_text_box.png)
Contact box|Show contact box on Desktop| Check to show on Desktop
^^|Show contact box on Mobile| Check to show in Mobile
^^|Show icon - Header style 2|![](/electro5/header_contact.png)
Header Links| Setting Store location & Track your oder link to page|![](/electro5/header_header_links.png)

## Navigation Configuration
Following next options related to `Navigation`.  
Please read more detail: 
[<button>Setup Navigation Mega Menu</button>](../customization/navigation.md#understanding-navigation)
![](mega_menu_add.gif)
### Sticky Menu

![](header_sticky_desktop.png)
![](header_sticky_mobile.png)

### Menu Fly Out & Highlight
In default, the mega navigation will flyout at direction at Right side of the drop-down menu. However, if you would like to change the direction flyout, you may add the menu titles(label) into `MENU FLY OUT LEFT` field. Add a separator ; for each title

- Eg. Menu name is Flyout Left -> input `Flyout Left`
- Menu name is: Contact Us -> input `Contact Us`
- Input Flyout Left; Contact us if you would like to show Flyout Left & Contact us sub menu at the left side
![](header_fly.png)
### Menu Highlight
Options to highlight the menu item in Horizontal & Verticle Menu.
![](header_highlight.png)
 -->
Updating