import Vue from 'vue'
import VTooltip from 'v-tooltip'

export default ({ Vue, isServer }) => {
  Vue.use(VTooltip);
  if (!isServer) {
    import('vue-toasted' /* webpackChunkName: "notification" */).then((module) => {
      Vue.use(module.default)
    })
  }
}