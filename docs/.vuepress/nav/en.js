module.exports = [
  {
    text: 'Essential Information',
    items: [
      {text: '1. Support Information',
      link: '/quickstart/'
      },
      {text: '2. License',
      link: '/quickstart/license.html'
      },
      {text: '3. Support Policy',
      link: '/quickstart/support.html'
      },
      {text: '4. Before You Submit The Support Ticket',
      link: '/quickstart/beforesubmit.html'
    }
  ]
},
  {
    text: 'Theme Installation',
    items: [
        {text: 'Step 1: Preparing Your Shopify Store',
        link: '/installation/#preparing-your-shopify-store/'
        },
        {text: 'Step 2: Locating Your Theme Purchase Code',
        link: '/installation/purchase-code.html'
        },
        {text: 'Step 3: Theme Installation',
        link: '/installation/dashboard.html'
        },
        {text: 'Step 4: How to Delete Duplicated Data',
        link: '/installation/dupdatadel.html'
        },
        {text: 'Step 5: How to Update your Theme',
        link: '/installation/update-how-to.html'
        }
      ]
  },
  {
    text: 'Theme User Guide',
    items: [
      {text: 'Quick Start Guide',
      link: '/customization/'
        },
      {text: '1. Integrating Shopify App',
      link: '/customization/apps.html'
        },
        {text: '2. Theme Customization Essentials',
      link: '/customization/startcustom.html'
        },
      {
        text: '3. Setup Mega Menu (Navigation)',
        link: '/customization/navigation.html'
        },
      {
        text: '4. Create and Customize Pages',
        link: '/customization/page.html'
        },
      {
        text: '5. Create and Customize Collections',
        link: '/customization/collection.html'
        },
      {
        text: '6. Create and Customize Products',
        link: '/customization/product.html'
        },
      {
        text: '7. Grid System & Breakpoint',
        link: '/customization/grid.html'
        },
      {
        text: '8. Currency & Multi Currency',
        link: '/customization/currency.html'
        },
      {
        text: '9. How To Use Custom Fields',
        link: '/customization/metafield.html'
        },
      {
        text: '10. How to Fomo 2.0',
        link: '/customization/fomo.html'
        },
      {
        text: '11. How to Add Media Files',
        link: '/customization/add-media.html'
        },
      {
        text: 'Electro 5.0+',
        items: [
          {
            text: 'User Guide',
            link: '/electro5/'
          }
        ]
      },
      {
        text: 'Xstore 1.0+',
        items: [
          {
            text: 'User Guide',
            link: '/xstore/'
          }
        ]
      },
      {
        text: 'Handy 4.0',
        items: [
          {
            text: 'User Guide',
            link: '/handy4/'
          } 
        ]
        },
      {
        text: 'Zeexo 1.0',
        items: [
          {
            text: 'User Guide',
            link: '/zeexo/'
          }
        ]
      }
    ]
  },
  {
    text: 'Support Portal',
    items: [
      {
        text: 'Our Offer',
        items: [
          {
            text: 'Create Support Ticket',
            link: 'https://arenathemes.freshdesk.com/support/tickets/new'
          },
          {
            text: 'Custom Shopify Theme Service',
            link: 'https://www.arenacommerce.com/pages/custom-your-shopify-theme'
          },
          {
            text: 'Free Shopify Theme Installation Service',
            link: 'https://www.arenacommerce.com/pages/free-installation-service'
          }
        ]
      }
    ]
  },
]
