const { fs, path } = require('@vuepress/shared-utils')
module.exports = ctx => ({
  base: '/',
  dest: 'public',
  title: 'ArenaCommerce',
  description: 'Welcome to your Knowledge Base Center',
  locales: {
    '/': {
      lang: 'en-US',
    }
  },
  markdown: {
    lineNumbers: false,
    anchor: { permalink: false },
    toc: { includeLevel: [2, 3] },
    externalLinks: {target: '_blank', rel: 'noopener noreferrer' },
    extendMarkdown: md => {
      md.set({
        html: true,
        linkify: true,
        typographer: true
         })
      md.use(require('markdown-it-multimd-table'), {enableMultilineRows: true, enableRowspan: true});
      md.use(require('markdown-it-task-lists'));
      md.use(require('markdown-it-sup'));
      md.use(require('markdown-it-video'));
      md.use(require('markdown-it-sub'));
    }
  },
  head: [
    ['link', { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.6.1/css/all.css' }],
    ['link', { rel: 'icon', href: `/favicon.png` }],
    ['link', { rel: 'manifest', href: '/manifest.json' }],
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    ['meta', { name: "viewport", content: "width=device-width, initial-scale=1.0, maximum-scale=1.0"}],
    ['link', { rel: 'apple-touch-icon', href: `/icons/apple-touch-icon-152x152.png` }],
    ['link', { rel: 'mask-icon', href: '/icons/safari-pinned-tab.svg', color: '#3eaf7c' }],
    ['meta', { name: 'msapplication-TileImage', content: '/icons/msapplication-icon-144x144.png' }],
    ['meta', { name: 'msapplication-TileColor', content: '#000000' }]
  ],
  theme: '@vuepress/default',
  themeConfig: {
    logo: '/logo.png',
    search: true,
    algolia: {
    apiKey: '78c97c1eecbaeb0f5c7f91324fbc99d7',
    indexName: 'arenacommerce'
    },
    editLinks: false,
    locales: {
      '/': {
        label: 'English',
        selectText: 'Languages',
        nav: require('./nav/en'),
        sidebar: {
          '/quickstart/': [
            '',
            'license',
            'support',
            'beforesubmit'
          ],
          '/installation/': [
            '',
            'purchase-code',
            'dashboard',
            'dupdatadel',
            'update-how-to'
          ],
          '/customization/': [
            '',
            'apps',
            'startcustom',
            'navigation',
            'page',
            'collection',
            'product',
            'grid',
            'currency',
            'metafield',
            'fomo'
          ],
          '/electro5/': [
            'changelog',
            'update',
            '',
            'general',
            'page-templates',
            'collections',
            'product-templates',
            'blogs',
            'header',
            'footer',
            'cart',
            'homepage',
            'howto'
          ],
          '/chromium/': [
            '',
            'general',
            'page-templates',
            'collections',
            'product-templates',
            'blogs',
            'header',
            'footer',
            'cart',
            'homepage',
            'howto',
            'development'
          ],
          '/handy4/': [
            '',
            'demo',
            'general',
            'typography',
            'nav',
            'collections',
            'product',
            'development'
          ],
          '/zeexo/': [
            '',
            'general',
            'upgrade',
            'navigation',
            'typography',
            'colors',
            'sections',
            'footer',
            'pages',
            'collection',
            'product',
            'swatch',
            // 'image',
            'changelog'
          ],
          '/admin-extension/': [
            '',
            'admin-bar',
            'advanced_editor',
            'content_editor',
            'url_filter'
          ],
          '/xstore/': [
            'changelog',
            'password',
            '',
            'purchase-code',
            'themeinstallprep',
            'themeinstall',
            'apps',
            // 'update',
            'dupdatadel',
            'demo',
            'commonterms',
            //'nav',
            'collection',
            'product',
            // 'page',
            'section',
            'blog',
            'featured'
          ]
        }
      },
    }
  },
  plugins: [
    ['@vuepress/search', {
      searchMaxSuggestions: 10,
    }],
    ['tabs'],
    ['flowchart'],
    ['@vuepress/medium-zoom', {
      selector: '.theme-default-content:not(.custom) img',
      options: {
        margin: 16,
        background: '#BADA55',
      }
    }],
    [
      {
        sidebarLinkSelector: '.sidebar-link',
        headerAnchorSelector: '.header-anchor',
        headerTopOffset: 120
      }
    ],
    ['@vuepress/back-to-top', true],
    ['@vuepress/pwa', {
      serviceWorker: true,
      popupComponent: 'MySWUpdatePopup',
      updatePopup: true}
    ],
    ['@vuepress/google-analytics', {
      ga: 'UA-141866471-2'
    }],
    ['container', {
      type: 'vue',
      before: '<pre class="vue-container"><code>',
      after: '</code></pre>',
    }],
    ['container', {
      type: 'upgrade',
      before: info => `<UpgradePath title="${info}">`,
      after: '</UpgradePath>',
    }],
    ['container', {
      type: 'right',
      defaultTitle: '',
    }],
    ['container', {
      type: 'theo',
      before: info => `<div class="theo"><p class="title">${info}</p>`,
      after: '</div>',
    }]
  ],
  extraWatchFiles: [
    '.vuepress/nav/en.js'
  ]
})