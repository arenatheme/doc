---
metaTitle: Theme Update Information - ArenaCommerce Help Center
sidebarDepth: 2
---

# Theme Update
The theme update process will differ by the methods you chose to install your theme. Please choose the methods listed below to see the details.  

:::tip Before updating your theme 
Please be aware that all your installed third-party apps, code modifications, '.js' and '.css' files will be valid for the theme you have made those modifications on. 
When you update your purchased theme, the updated theme will be available as a different entity than the old version of itself.
This means when you switch to the updated theme, these modifications will not be apply for this newly updated theme and you will have to make the same modifications again.
:::  

:::: tabs cache-lifetime="10" :options="{ useUrlFragment: false }"
::: tab "Arena Dashboard" id="arena-dashboard-update"
You will get notify when we have new update for any licensed theme you added. Flowing process as Install.
:::
::: tab "Manually" id="manual-update"
When you manually add an updated version of a theme, a second version of the theme is added to your online store. The new version is in its default state, with **none of your customizations applied**. The older version is unaffected by the update, and still has all of your code customizations.  
1. [Create a backup copy of your existing theme.](https://help.shopify.com/en/manual/using-themes/managing-themes/duplicating-themes)
2. Download the new theme file from ThemeForest or install our Arena Installation App.
3. [Install the new version](./#manual-upload) as new theme for your store.
:::
::::