---
metaTitle: Using Metafields in Your Shopify Theme - ArenaCommerce Help Center
sidebarDepth: 2
---
# Using Custom Fields in Your Shopify Theme

You can use metafields to add custom fields to objects such as products, customers, and orders. Metafields are useful for storing specialized information, such as part numbers, customer titles, or blog post summaries. They can be used by apps and channels to track data for internal use.

**Advanced Custom Field (ACF)** a powerful application for your Shopify store that utilizes Shopify’s Metafields. It enables admins to add structured metafield fields (custom fields) to things in Shopify (objects), such as products, collections, orders, customers, and pages. These extra content fields are more commonly referred to as Custom Fields and can allow you to build websites faster and educate your clients quicker.

More detail [https://help.shopify.com/en/manual/products/metafields](https://help.shopify.com/en/manual/products/metafields)

![](/acf/acf.jpg)


---

## 1. Installing Advanced Custom Field (ACF)
![](/acf/acf_install.gif)


- **<u>Step 1</u>**: Go to [Advanced Custom Field](https://apps.shopify.com/advanced-custom-field) in the Shopify App Store after you logged in to your myShopify account.  
- **<u>Step 2</u>**: Click the **`Add App`** button.  
- **<u>Step 3</u>**: Log into the App Store.  
- **<u>Step 4</u>**: Confirm App installation by click **`Install App`**.  
- **<u>Step 5</u>**: Use the app! Your newly installed app will live in the App section of your Shopify Admin.

## 2. Using ACF to extend Your Store Content
@[youtube](pNYMohlbFLA)
### Quick Start
**ACF** supports import the **default metafields that we use in most themes.**  
- **<u>Step 1</u>**: Go to Apps > **Advanced Custom Field** > **Fields** > **Products**  
- **<u>Step 2</u>**: Click **Import** Button  
- **<u>Step 3</u>**: Click **Save** Button  
Now you can go to Editor tab to edit your product custom fields content.  
![](/acf/acf_import.gif)  

::: tip Note
- All of the following metafields are for resource **Products**.
- To know which metafields in the list below can be used in your ArenaCommerce theme(s), please refer to corresponding theme's documentation or [contact us](https://arenathemes.freshdesk.com/support/tickets/new).
:::

**You may also following instruction below to add custom field manually**

### Product Short Description / Exception Description
- **<u>Step 1</u>**: At Field tab, create a HTML FIeld Product Description.
![](/acf/acf_description.gif)
::: right
How to [add new field](/acf/fields-tab.html#add-new-field).  	
:::
- **<u>Step 2</u>**: Click **Save** button to save your changes; then go to Editor tab’s resource “Products” and pick a product to start with.
::: right
Use search bar to quickly [find specific products](/acf/search.html#products).
:::

```
- namespace: c_f
- key: description_excerpt
- label: Short Description
```
- <u>**Step 3**</u>: Click **Save** button to save your changes; then go to Editor tab's resource "*Products*" and pick a product to start with.
![](/acf/acf_description_editor.gif)


- <u>**Step 4**</u>: On next page, pick a product item for each field editor via resource picker. Add your product short description.
![](/acf/bundle-product.png)  
- <u>**Step 5:**</u> Click **Save** button to save your changes.  
::: right
***Use left - right arrow buttons to navigate to previous - next items.***
:::

### Countdount Timer
- <u>**Step 1**</u>: At Field tab, create a [date field](/acf/types-of-fields.html#_7-date)

	```
	- namespace: c_f
  	- key: countdown_timer
  	- label: Date format is mm/dd/yyyy
  	```
It is required to set **date format** as *mm/dd/yyyy* in order for countdown to work.  
- <u>**Step 2**</u>: Click **Save** button to save your changes; then go to Editor tab's resource "*Products*" and pick a product to start with.
- <u>**Step 3**</u>: On next page, pick a date 
- <u>**Step 4**</u>: Click **Save** button to save your changes.  
**Countdount Timer** is all set up. 

### Bundle Product 
- <u>**Step 1**</u>: At Field tab, [create a group](/acf/fields-tab.html#add-new-group) called **Bundle Product**.
- <u>**Step 2**</u>: Create 2 [product reference fields](/acf/types-of-fields.html#_13-product) in that group

	```
	- namespace: c_f
  	- key: bundle_1
  	- label: Add your product handle #1!
  	```

  	```
  	- namespace: c_f
  	- key: bundle_2
  	- label: Add your product handle #2!
  	```
- <u>**Step 3**</u>: Click **Save** button to save your changes; then go to Editor tab's resource "*Products*" and pick a product to start with.


- <u>**Step 4**</u>: On next page, pick a product item for each field editor via resource picker.
![](/acf/bundle-product.png) 

::: right
Use left - right arrow buttons to navigate to previous - next items.
:::
- <u>**Step 5**</u>: Click **Save** button to save your changes. 

And... That's it! **Bundle Product** is all set up. You can refer to our example work at https://electro.arenacommerce.com/products/gold-diamond-chain


### Exit Intent Popup Product 
- <u>**Step 1**</u>: At Field tab, [create a group](/acf/fields-tab.html#add-new-group) called **Exit Intent Popup Product**.
- <u>**Step 2**</u>: Create 2 [text fields](/acf/types-of-fields.html#_1-text) in that group
	
	```
	- namespace: c_f
  	- key: discount_code
  	- label: Add your Coupon code for the product
  	```

  	```
  	- namespace: c_f
  	- key: discount_percent
  	- label: Display percent of discount
  	```

- <u>**Step 3**</u>: Click **Save** button to save your changes; then go to Editor tab's resource "*Products*" and pick a product to start with.
- <u>**Step 4**</u>: On next page, add your coupon code and display percent of discount
- <u>**Step 5**</u>: Click **Save** button to save your changes. 

**Exit Intent Popup Product** is all set up. You can refer to our example work at https://electro.arenacommerce.com/products/black-fashion-zapda-shoes

## Bulk editing product metafield

As we’ve seen, metafields have three components: `a namespace, a key, and a value`. Once you know the namespace and key for your metafield, you can display it in [the bulk editor](https://help.shopify.com/en/manual/productivity-tools/bulk-editing-products) by making small changes to the URL of the bulk editing page in the Shopify Admin.

Eg. Collection page: [https://shopify.com/admin/bulk?resource\_name=Collection&edit=metafields.c\_f.subcategory:string](https://shopify.com/admin/bulk?resource_name=Collection&&edit=metafields.c_f.subcategory:string)

In this URL, `c\_f` is the `metafield’s namespace` and `subcategory` is its key. You can replace these with values to match your use case. The `string` part of the URL tells Shopify about the type of data that we want to store in the metafield. The bulk editor is able to display five types of metafields – `string`, `money`, `boolean`, `select` and `number`