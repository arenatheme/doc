---
metaTitle: Preparing your Shopify Store - ArenaCommerce Help Center
---
# Preparing your Shopify Store
Before installing the theme to start working on your new store, first you have to make some configurations to avoid unexpected problems, mainly currencies and customer account login related.  
**<span style="color:purple"> Please strictly follow the steps described in this chapter before you go to the next.</span>**

### <i class="fa fa-user-circle" aria-hidden="true"></i> Enable Account Login

For customer to start buying your products, first they need to have an account. To enable account login, please do the following:  
- From your myShopify Admin Homepage, go to `Settings` / `Checkout`.
![Checkout](/xstore/settings-checkout.png)
- In the block **Customer accounts** select **Accounts are optional**.
![](/xstore/checkout-choice-prep.png)
- Click **Save**.

### <i class="fa fa-money-bill-wave"></i> Configure Currency

Your store's currency is the currency that you have set up on the [**General settings**](https://www.shopify.com/admin/settings/general) page. This currency will be displayed throughout products on your store. To configure the currency you want to display, please do the following:  
1. From your myShopify Admin Homepage, click on `Settings` > Choose `General`.
![](/settings-general.png)
2. In the `Store currency` section, click the `Change formatting` link.
![currency formatting](/xstore/format-currency.png)
3. In the `HTML with currency field`, wrap the existing content in an HTML span element with a class name set to `money`.  
E.g. If your store currency is `European Monetary Unit (EUR)`, currency formatting is:  
    ```html
    <span class=money>€{{amount}} EUR</span>
    ```  

4. In the HTML without currency field, enter the exact same content that you entered in the HTML with currency field. Adding the currency descriptor to both formats will help to differentiate between currencies that use the dollar sign (€).  
    Eg. If your store currency is `European Monetary Unit (EUR)`, currency formatting is:   
    ```html
    <span class=money>€{{amount}}</span>
    ```
    ![Currency Format](/xstore/format-currency2.png)  
5. Click **Save**.  
:::warning
Do not use quotation marks around the word 'money', as adding them in might result in JavaScript errors.
:::  
::: right
For more details, please go to Shopify Official Source's [Shopify Currency Setting](https://help.shopify.com/en/manual/using-themes/change-the-layout/help-script-find-money-formats).
:::  