---
title: ArenaCommerce Help Center
metaTitle: Shopify Theme Documentation - ArenaCommerce Help Center
home: true
heroImage: /hero.png
heroText: Arena Knowledge Base
tagline: Build your store faster and explore all the powerful features that our theme designs included
actionText: Quick Install Guide
actionLink: /installation/dashboard.html
footer: ArenaCommerce | Copyright © 2014-present
---

### *Thank you for choosing Arena!*

*You must be excited about starting your new project, so let's just get on with it. It won't be long until you get to the point when you can actually start working on your site. In this user guide, you will find all the information you need to get your site up and running: starting from theme installation and setup to usage of different functions such as Products, Collections, etc... The following steps will get you there.*

Our themes come with complimentary support. So when all else fail, please never hesitate to submit a support request via our [Support Portal](https://support.arenathemes.com)

### How to use this document
Please read this user guide carefully, it will help you eliminate most of potential problems with incorrect configuration of the theme
- Quick Start: Support & Policy Information
- Installation: General ArenaCommerce Shopify Theme Installation
- Theme Customization: ArenaCommerce Shopify Basic Theme Customization
- Specific Theme Customization

Use the Search bar on top to quickly find all the keywords  you are looking for.

### <i class="fas fa-star"></i> Rate us!
Don't forget to rate our theme on ThemeForest – it's good to know what others think about
our work. You can rate it in the download area of your account on ThemeForest.
